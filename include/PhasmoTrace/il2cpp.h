#ifndef _IL2CPP_H
#define _IL2CPP_H
#include "QuickDebug.hpp"



namespace Il2Cpp {
namespace BuiltIn {
struct Empty {};
struct Il2CppObject {
    QuickDebug::Addr klass = 0;
    QuickDebug::Addr monitor = 0;
};
struct Il2CppArrayBounds {
    size_t length;
    int lower_bound;
};
struct Il2CppArray {
    Il2CppObject obj;
    QuickDebug::Addr arrayBounds;
    size_t max_length;
    QuickDebug::Addr first_item;
};
}

#pragma pack(push)
namespace System {
#pragma pack(8)
struct String {
    int length;
    char firstChar;
};
namespace Collections {
namespace Generic {
struct List {
    QuickDebug::Addr _items;
    int _size;
    int _version;
    QuickDebug::Addr _syncRoot;
};
struct Dictionary {
    struct Entry {
        int32_t hashCode;
        int32_t next;
        QuickDebug::Addr key;
        QuickDebug::Addr value;
    };
    QuickDebug::Addr buckets;
    QuickDebug::Addr entries;
    int count;
    int version;
    int freeList;
    int freeCount;
    QuickDebug::Addr comparer;
    QuickDebug::Addr keys;
    QuickDebug::Addr values;
    QuickDebug::Addr _syncRoot;
};
}
}
}

namespace UnityEngine {
struct Object {
    intptr_t cachedPtr;
};
}
#pragma pack(pop)

template<class Fields>
struct Object {
    BuiltIn::Il2CppObject object;
    Fields fields;
};

namespace UnityEngine {
struct Vector2 {
    float x, y;
};
struct Vector3 {
    float x, y, z;
};
struct Quaternion {
    float x, y, z, w;
};
struct Color {
    float r, g, b, a;
};
struct Color32 {
    int32_t rgba;
    uint8_t r, g, b, a;
};
}
}
#endif // _IL2CPP_H
