#ifndef _PHASMOENGINE_GHOSTIDENTITY_HPP
#define _PHASMOENGINE_GHOSTIDENTITY_HPP
#include "SimpleTypes.hpp"

#include <string>
#include <string_view>
#include <memory>



namespace PhasmoEngine {
struct GhostIdentity {
    std::string_view firstName, lastName;
    unsigned age;
    enum Gender : bool {male = 0, female = 1} gender;
    GhostType type;
    float agression;
    Sound *huntSound;
    std::unique_ptr<class GhostBehavior> behavior;

    GhostIdentity() {}
    void generate(LevelManager *levelManager, unsigned seed);

    std::string getFullName() const {
        return std::string(firstName) + ' ' + std::string(lastName);
    }
};

}
#endif
