#include "PhasmoTrace/Tracer.hpp"
#include "PhasmoTrace/TracerCapstone.hpp"
#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"
#include "capstone/capstone.h"
#include "pe-parse/parse.h"

#include <fstream>
#include <filesystem>
#include <thread>
#include <simdjson.h>
#include <GLFW/glfw3.h>



namespace PhasmoTrace {
void Tracer::run(QuickDebug::pid_t pid, const char *gameassembly_path) {
    // Initialize il2cpp helpers
    initializeIl2CppHelpers(gameassembly_path);
    // Get list of plugins
    auto pluginPrefix = "libPT_plugin-"+std::string(game.getName());
    for (const auto& file : std::filesystem::directory_iterator(std::filesystem::current_path())) {
        if (file.path().filename().string().find(pluginPrefix) == 0) {
            plugins.push_back(PluginDesc(file.path().string()));
        }
    }
    // Attach to process
    target.attach(pid);
    logger.log(Loglevel::info, fmt::format("Attached to PID {}", pid));
    // Add callbacks
    target.onUnknownBreakpoint.push_back([this] () -> bool { // We've found the initial breakpoint
        resetTempBufferSpaceAllocs();
        // Get RIP
        auto rip = target.getRegisters().rip;
        if (!breakpointsReady && target.readByte(rip) == SimpleInstructions::breakpoint) {
            // Look for initial breakpoint function in our function list
            const auto& initialBrkFunc = std::find_if(knownFunctions.begin(), knownFunctions.end(), [this] (auto a) {
                return a.name == game.getInitialFunction();
            });
            // Check RIP ending
            if ((initialBrkFunc->addr & 0xffff) != (rip & 0xffff)) {
                logger.log(Loglevel::info, "Thought I had found the base address but I didn't");
                return false;
            }
            // Write return instruction
            target.writeByte(rip, SimpleInstructions::ret);
            // Calculate addr base
            addrBase = rip - initialBrkFunc->addr;
            // We'll use the space occupied by this function as buffer space – let's hope nothing goes wrong
            bufferSpace = addrBase + initialBrkFunc->addr + sizeof(QuickDebug::SimpleInstructions::ret);
            // Log
            logger.log(Loglevel::info, fmt::format("Found addr base at {}", addrBase));
            // Add all our breakpoints to target
            for (const auto& function : knownFunctions) {
                auto brk = target.addBreakpoint(function.addr + addrBase, function.name);
                brk->getData() = &function;
            }
            // Activate PlayerUpdate breakpoint
            target.activateBreakpoint(target.getBreakpointsByName("Player$$Update")[0]);
            // Mark breakpoints as ready
            breakpointsReady = true;
            // Log
            logger.log(Loglevel::info, "All breakpoints have been configured");
            // We've processed this breakpoint, carry on
            return true;
        }
        // We haven't been able to process this breakpoint
        return false;
    });
    target.onBreakpoint.push_back([this] (BreakpointList& breakpoints) {
        resetTempBufferSpaceAllocs();
        auto& breakpoint = breakpoints[0];
        bool isUpdateBreakpoint = breakpoint.getName().find("$$Update") != std::string::npos;
        // Special update breakpoint handling
        if (isUpdateBreakpoint && !onUpdateQueue.empty() && breakpoint.getName() == "Player$$Update") { // Process onUpdate queue
            std::scoped_lock L(onUpdateQueueMutex);
            onUpdateQueue.front()();
            onUpdateQueue.pop();
        } else { // Run plugin breakpoint callbacks
            for (auto& plugin : plugins) {
                if (plugin.isLoaded()) {
                    auto cb = plugin->get<void(BreakpointList&)>("onBreakpoint");
                    if (cb) {
                        try {
                            cb(breakpoints);
                        } catch (std::exception& e) {
                            auto msg = fmt::format("Error executing plugin {}: {}", plugin->getName(), e.what());
                            logEvent(msg);
                            logger.log(Loglevel::error, msg);
                        }
                    }
                }
            }
        }
        // Log
        if (!isUpdateBreakpoint) { // Don't log update calls
            auto regs = target.getRegisters();
            logEvent(fmt::format("Call -> {}({}, {}, {}, {})", breakpoint.getName(), getValueString(regs.rcx), getValueString(regs.rdx), getValueString(regs.r8), getValueString(regs.r9)));
            if (breakpoint.getData().type() == typeid(const PhasmoTrace::Function*)) {
                const auto& signature = std::any_cast<const PhasmoTrace::Function*>(breakpoint.getData())->signature;
                logEvent(fmt::format("      > {}", signature));
            }
        }
    });
    target.onPostBreakpoint.push_back([this] (BreakpointList& breakpoints) {
        resetTempBufferSpaceAllocs();
        // Run action queue
        runActionQueue();
        // Run plugin post breakpoint callbacks
        for (auto& plugin : plugins) {
            if (plugin.isLoaded()) {
                auto cb = plugin->get<void(BreakpointList&)>("onPostBreakpoint");
                if (cb) {
                    cb(breakpoints);
                }
            }
        }
    });
    // Run
    while (target.isAttached()) {
        try {
            target.runOnce();
        } catch (QuickDebug::Exception &e) {
            auto msg = fmt::format("Tracer error: {}", e.what());
            logger.log(Loglevel::error, msg);
            logEvent(msg);
            target.continue_();
        }
    }
    // We're done
    logger.log(Loglevel::info, "Tracer has exited without an error");
}

struct PEExportCb_data {
    Il2CppHelpers& helpers;
    peparse::VA baseAddr;
};
int PEExportCb(void *_data,
               const peparse::VA &funcAddr,
               std::uint16_t ordinal,
               const std::string &mod,
               const std::string &func,
               const std::string &fwd) {
    auto data = reinterpret_cast<PEExportCb_data*>(_data);

    if (func == "il2cpp_class_from_name") {
        data->helpers.il2cpp_class_from_name = funcAddr - data->baseAddr;
    } else if (func == "il2cpp_value_box") {
        data->helpers.il2cpp_value_box = funcAddr - data->baseAddr;
    } else if (func == "il2cpp_get_corlib") {
        data->helpers.il2cpp_get_corlib = funcAddr - data->baseAddr;
    }

    return 0;
}
void Tracer::initializeIl2CppHelpers(const char *path) {
    auto pe = peparse::ParsePEFromFile(path);
    PEExportCb_data data{il2CppHelpers, pe->peHeader.nt.OptionalHeader64.ImageBase};
    IterExpFull(pe, PEExportCb, &data);
    DestructParsedPE(pe);

    logger.log(Loglevel::info, fmt::format("Located il2cpp_class_from_name @ {} and il2cpp_value_box @ {}", reinterpret_cast<void*>(il2CppHelpers.il2cpp_class_from_name), reinterpret_cast<void*>(il2CppHelpers.il2cpp_value_box)));
}

QuickDebug::Addr Tracer::getIl2CppKlass(const std::string& namespaze, const std::string& name) {
    // Generate assembly
    std::string asmStr = fmt::format("push rcx\n"
                                     "push rdx\n"
                                     "mov rax, {}\n"
                                     "sub rsp, 0x20\n"
                                     "call rax\n"
                                     "add rsp, 0x20\n"
                                     "mov rcx, rax\n"
                                     "pop r8\n"
                                     "pop rdx\n"
                                     "mov rax, {}\n"
                                     "sub rsp, 0x20\n"
                                     "call rax\n"
                                     "add rsp, 0x20\n"
                                     "ret", il2CppHelpers.il2cpp_get_corlib + addrBase, il2CppHelpers.il2cpp_class_from_name + addrBase);
    auto asmBin = assemble(asmStr);
    auto codeBuf = allocateTempBufferSpace(asmBin.size());
    target.writeBuffer(codeBuf, asmBin.data(), asmBin.size());

    // Back up state
    auto regs = target.getRegisters();
    auto origRegs = regs;
    auto origTempBuffer = bufferSpaceAllocOff;
    // Prepare code execution
    auto namespazeBuf = allocateTempBufferSpace(namespaze.size()+1);
    target.writeBuffer(namespazeBuf, namespaze.c_str(), namespaze.size()+1);
    auto nameBuf = allocateTempBufferSpace(name.size()+1);
    target.writeBuffer(nameBuf, name.c_str(), name.size()+1);
    regs.rcx = namespazeBuf;
    regs.rdx = nameBuf;
    regs.rip = codeBuf;
    target.setRegisters(regs);
    // Execute code
    continueUntilReturn();
    // Restore state
    auto fres = target.getRegisters().rax;
    target.setRegisters(origRegs);
    bufferSpaceAllocOff = origTempBuffer;
    // Final result
    return fres;
}
QuickDebug::Addr Tracer::boxIl2CppValue(const std::string& namespaze, const std::string& name, QuickDebug::Addr valueBuf) {
    auto regs = target.getRegisters();
    regs.rcx = getIl2CppKlass(namespaze, name);
    regs.rdx = valueBuf;
    regs.rip = il2CppHelpers.il2cpp_value_box + addrBase;
    continueUntilReturn();
    return target.getRegisters().rax;
}

void Tracer::retOut(std::shared_ptr<QuickDebug::Breakpoint>& brk) {
    target.writeByte(brk->getAddr(), QuickDebug::SimpleInstructions::ret);
}
void Tracer::retOutWValue(std::shared_ptr<QuickDebug::Breakpoint>& brk, QuickDebug::Word value) {
    retOut(brk);
    auto regs = target.getRegisters();
    regs.rax = value;
    target.setRegisters(regs);
}

std::shared_ptr<QuickDebug::Breakpoint> Tracer::findBreakpointBySignature(std::string_view signature) noexcept {
    for (const auto& brkl : target.getBreakpoints()) {
        for (const auto& brk : brkl.second.getVector()) {
            if (std::any_cast<const PhasmoTrace::Function*>(brk->getData())->signature == signature) {
                return brk;
            }
        }
    }
    return nullptr;
}

void Tracer::forEachBreakpointWithStrInName(std::string_view str, const std::function<void(const std::shared_ptr<QuickDebug::Breakpoint>&)>& cb) noexcept {
    for (auto& [addr, brkl] : target.getBreakpoints()) {
        for (auto& brk : brkl.getVector()) {
            if (brk->getName().find(str) != std::string::npos) {
                cb(brk);
            }
        }
    }
}
void Tracer::activateEachBreakpointWithStrInName(std::string_view str) noexcept {
    for (auto& [addr, brkl] : target.getBreakpoints()) {
        for (auto& brk : brkl.getVector()) {
            if (brk->getName().find(str) != std::string::npos) {
                addToActionQueue([this, brk] () {
                    target.activateBreakpoint(brk);
                });
            }
        }
    }
}
void Tracer::activateBreakpointWithName(std::string_view str) noexcept {
    addToActionQueue([this, str = std::string(str)] () {
        auto brks = target.getBreakpointsByName(str);
        if (!brks.empty()) {
            target.activateBreakpoint(brks[0]);
        }
    });
}
void Tracer::activateBreakpointWithSignature(std::string_view str) noexcept {
    addToActionQueue([this, str = std::string(str)] () {
        target.activateBreakpoint(findBreakpointBySignature(str));
    });
}
void Tracer::activateAllBreakpointsWithName(std::string_view str) noexcept {
    addToActionQueue([this, str] () {
        for (const auto& brk : target.getBreakpointsByName(str)) {
            target.activateBreakpoint(brk);
        }
    });
}
void Tracer::deactivateBreakpointWithName(std::string_view str) noexcept {
    addToActionQueue([this, str = std::string(str)] () {
        auto brks = target.getBreakpointsByName(str);
        if (!brks.empty()) {
            target.deactivateBreakpoint(brks[0]);
        }
    });
}
void Tracer::deactivateAllBreakpointsWithName(std::string_view str) noexcept {
    addToActionQueue([this, str] () {
        for (const auto& brk : target.getBreakpointsByName(str)) {
            target.deactivateBreakpoint(brk);
        }
    });
}

void Tracer::findFunctionPointers(Addr addr, size_t size, const std::vector<FunctionPointerFinder>& finders) {
    // Run capstone
    PhasmoTrace::Capstone cs(target, addr, size);
    // Read assembly
    unsigned calls_passed = 0;
    for (auto instr_it = 0; instr_it != cs.instr_count; instr_it++) {
        const auto& instr = cs.instrs[instr_it];
        // Check for call instruction
        if (instr.id == X86_INS_CALL) {
            // Get address
            auto caddr = instr.detail->x86.operands[0].imm;
            for (const auto& finder : finders) {
                switch (finder.identifier.index()) {
                case 0: {
                    if (std::get<unsigned>(finder.identifier) == calls_passed) {
                        finder.target = caddr;
                    }
                } break;
                case 1: {
                    auto brkl = target.getBreakpointsAt(caddr);
                    for (const auto& brk : brkl.getVector()) {
                        if (brk->getName().find(std::get<std::string_view>(finder.identifier)) == 0) {
                            finder.target = caddr;
                            break;
                        }
                    }
                } break;
                }
            }
            // Increment our call counter
            calls_passed++;
        }
        // Stop at padding
        else if (instr.id == X86_INS_INT3) {
            break;
        }
    }
}

void Tracer::overrideBranches(Addr addr, size_t size, const std::vector<bool>& overrides) {
    // Run capstone
    PhasmoTrace::Capstone cs(target, addr, size);
    // Read assembly
    auto override = overrides.begin();
    for (auto instr_it = 0; instr_it != cs.instr_count; instr_it++) {
        const auto& instr = cs.instrs[instr_it];
        // Check for conditional jump instruction (only the most common ones for now)
        if (instr.id == X86_INS_JE || instr.id == X86_INS_JNE) {
            // Overwrite instruction if requested
            if (*override) {
                std::vector<uint8_t> nops(instr.size);
                for (auto& b : nops) b = QuickDebug::SimpleInstructions::nop;
                target.writeBuffer(instr.address, nops.data(), nops.size());
            }
            // Next override
            if (++override == overrides.end()) {
                break;
            }
        }
        // Stop at padding
        else if (instr.id == X86_INS_INT3) {
            break;
        }
    }
}

void Tracer::logEvent(std::string_view message) noexcept {
    tracerLog += message;
    tracerLog += '\n';
    logger.log(Loglevel::info, fmt::format("Tracer log += \"{}\"", message));
}

void Tracer::loadPlugin(const std::string& fname) {
    auto& plugin = plugins.emplace_back(PluginDesc{fname});
    plugin.load(*this);
    logger.log(Loglevel::info, fmt::format("Loaded plugin: {} ({})", plugin->getName(), plugin->getVersion()));
}

size_t Tracer::storeStringAt(Addr addr, std::string_view str, QuickDebug::Addr klass) {
    size_t fres;
    Il2Cpp::Object<Il2Cpp::System::String> buffer;
    buffer.object.klass = klass;
    buffer.fields.length = str.size();
    target.writeObject(addr, buffer);
    auto offset = addr + reinterpret_cast<Addr>(&buffer.fields.firstChar) - reinterpret_cast<Addr>(&buffer);
    fres = offset;
    for (size_t i = 0; i != str.size(); i++) {
        fres = offset+i*2;
        target.writeByte(fres, str[i]);
        target.writeByte(fres+1, '\0');
    }
    fres = offset+str.size()*2;
    target.writeByte(fres, '\0');
    target.writeByte(fres+1, '\0');
    return fres-addr+2;
}

std::string Tracer::getStrAt(Addr addr) {
    auto str_o = target.readObject<Il2Cpp::Object<Il2Cpp::System::String>>(addr);
    if (str_o.fields.length == 0 || str_o.fields.length > 50) {
        return "";
    }
    std::string fres;
    fres.reserve(str_o.fields.length);
    auto str_o_firstChar_off = reinterpret_cast<Addr>(&str_o.fields.firstChar) - reinterpret_cast<Addr>(&str_o);
    for (decltype(str_o.fields.length) i = 0; i != str_o.fields.length*2; i += 1) {
        auto byte = target.readByte(addr + i + str_o_firstChar_off);
        if (byte != '\0') {
            fres.push_back(byte);
        }
    }
    return fres;
}

std::string Tracer::getValueString(uint64_t addr) {
    std::string fres;
    // Try to get as string
    try {
        fres = '"'+getStrAt(addr)+"\"@"+std::to_string(addr);
    } catch (std::exception&) {}
    // Check if string is valid
    bool valid = !fres.empty();
    if (valid) {
        for (const auto c : fres) {
            if (!isprint(c)) {
                valid = false;
                break;
            }
        }
    }
    // If invalid now, just convert the number
    if (!valid) {
        fres = std::to_string(addr);
    }
    // Return final value
    return fres;
}

std::vector<uint8_t> Tracer::generateABICall(QuickDebug::Addr fnc, const std::vector<QuickDebug::Word>& args, std::function<void (std::ostringstream &)> beforeCallGen, std::function<void (std::ostringstream &)> beforeReturnGen) {
    // Initialize
    std::ostringstream assembly;
    // Backup ABI registers
    assembly << "push rax\n"
                "push rcx\n"
                "push rdx\n"
                "push r8\n"
                "push r9\n"
                "push r10\n"
                "sub rsp, 0x20\n";
    // Callback
    if (beforeCallGen) {
        beforeCallGen(assembly);
    }
    // Arguments
    size_t stackArgs = (args.size()<4)?(0):(args.size()-4);
    for (unsigned it = 0; it != args.size() && it < 4; it++) {
        const auto& arg = args[it];
        // Pass each value
        const static std::vector<std::string_view> registers = {"rcx", "rdx", "r8", "r9"};
        if (it < registers.size()) {
            assembly << "mov " << registers[it] << ", 0x" << std::hex << arg << '\n';
        }
    }
    size_t stackDepth = stackArgs * sizeof(QuickDebug::Word);
    size_t stackAlignmentExtra = 16-stackDepth%16 - sizeof(QuickDebug::Word);
    if (stackAlignmentExtra != 16) {
        assembly << "sub rsp, 0x" << std::hex << stackAlignmentExtra << '\n';
        stackDepth += stackAlignmentExtra;
    }
    if (args.size() >= 4) {
        for (unsigned it = args.size()-1; it != 3; it--) {
            assembly << "push 0x" << std::hex << args[it] << '\n';
        }
    }
    // Call function
    assembly << "mov r10, 0x" << std::hex << fnc << "\n"
                "call r10\n";
    // Unwind stack
    if (stackDepth) {
        assembly << "add rsp, 0x" << std::hex << stackDepth << '\n';
    }
    // Callback
    if (beforeReturnGen) {
        beforeReturnGen(assembly);
    }
    // Restore ABI registers
    assembly << "add rsp, 0x20\n"
                "pop r10\n"
                "pop r9\n"
                "pop r8\n"
                "pop rdx\n"
                "pop rcx\n"
                "pop rax\n"
    // Return
                "ret\n";
    // Get the entire thing as string
    auto asm_str = std::move(assembly).str();
    // Assemble
    std::vector<uint8_t> fres = assemble(asm_str);
    // Return final result
    return fres;
}

std::vector<uint8_t> Tracer::assemble(std::string_view assembly) {
    static std::mutex mutex;
    std::scoped_lock lock(mutex);
    std::fstream file;
    file = std::fstream(".assembly.s", std::ios_base::openmode::_S_out);
    file << "bits 64\n";
    file.write(assembly.data(), assembly.size());
    file.close();
    if (system("nasm .assembly.s -O0 -o .assembly.bin") != 0) {
        logger.log(Loglevel::error, "Assembler failed, emmiting dump return");
        return {QuickDebug::SimpleInstructions::ret};
    }
    auto binary_size = std::filesystem::file_size(".assembly.bin");
    file = std::fstream(".assembly.bin", std::ios_base::openmode::_S_in | std::ios_base::openmode::_S_bin);
    std::vector<uint8_t> binary(binary_size);
    file.read(reinterpret_cast<char*>(binary.data()), binary.size());
    file.close();
    return binary;
}

void Tracer::call(std::string_view fnc, const std::vector<QuickDebug::Word>& args) {
    auto brkl = target.getBreakpointsByName(fnc);
    if (brkl.empty()) {
        logger.log(Loglevel::error, fmt::format("Tried to call into non-existent function: {}", fnc));
        return;
    }
    call(brkl[0]->getAddr(), args);
}
void Tracer::call(QuickDebug::Addr fnc, const std::vector<QuickDebug::Word>& args) {
    auto code = generateABICall(fnc, args);
    auto codeBuf = getTempBufferSpace();
    target.writeBuffer(codeBuf, code.data(), code.size());
    lateAllocateTempBufferSpace(code.size());
    auto regs = target.getRegisters();
    regs.rip = codeBuf;
    target.setRegisters(regs);
}

void Tracer::continueUntilReturn() {
    // Back up start registers
    auto startRegs = target.getRegisters();
    // Step function until it returns
    for (;;) {
        // Singlestep
        target.singleStep();
        // Get registers
        const auto& regs = target.getRegisters();
        // Check if function is returning
        if (startRegs.rsp == regs.rsp && target.readByte(regs.rip) == QuickDebug::SimpleInstructions::ret) {
            return;
        }
    }
}

void Tracer::loadScript(std::string_view filename) {
    logger.log(Loglevel::info, "Processing script file...");
    simdjson::ondemand::parser parser;
    auto json = simdjson::padded_string::load(filename);
    auto scriptJson = parser.iterate(json);
    // Get functions
    unsigned errors = 0;
    for (auto functionJson : scriptJson["ScriptMethod"]) {
        try {
            Function brk;
            brk.addr = functionJson["Address"];
            brk.name = std::string_view(functionJson["Name"]);
            brk.signature = std::string_view(functionJson["Signature"]);
            // Add to our list
            knownFunctions.push_back(brk);
        } catch (...) {
            // Just ignore it
            errors++;
        }
    }
    // Log out function count
    if (errors) {
        logger.log(Loglevel::info, fmt::format("Processed {} functions and failed on {} function from script file", knownFunctions.size(), errors));
    } else {
        logger.log(Loglevel::info, fmt::format("Processed {} functions from script file", knownFunctions.size()));
    }
}

void Tracer::UIBreakpointButton(const std::shared_ptr<Breakpoint>& breakpoint) noexcept {
    // Button with function name
    if (ImGui::Button((fmt::format("{} {}", breakpoint->isActive() ? '-' : '+', breakpoint->getName())).c_str())) {
        // Make sure we're not messing with the PlayerUpdate breakpoint!!!
        if (breakpoint->getName() != "Player$$Update") {
            // Activate breakpoint there
            addToActionQueue([this, &breakpoint] () mutable {
                if (breakpoint->isActive()) {
                    target.deactivateBreakpoint(breakpoint);
                } else {
                    target.activateBreakpoint(breakpoint);
                }
            });
        }
    }
}

void Tracer::runUI() noexcept {
    // Initialize GLFW
    glfwInit();
    logger.log(Loglevel::info, "UI: GLFW initialized");
    // Define GLSL version to use
    const char* glsl_version = "#version 130";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    // Create window
    GLFWwindow* window = glfwCreateWindow(1024, 768, "PhasmoTrace", NULL, NULL);
    if (!window) {
        logger.log(Loglevel::error, "UI: Failed to initialize: GLFW failed to create window");
        return;
    }
    logger.log(Loglevel::info, "UI: Window created");
    // Configure window
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1); // Enable vsync
    // Setup ImGUI context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    // Set stype to dark colors
    ImGui::StyleColorsDark();
    // Setup renderer stuff
    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init(glsl_version);
    logger.log(Loglevel::debug, "UI: Window and renderer configured");
    // Set clear color
    constexpr ImVec4 clear_color(0.45f, 0.55f, 0.60f, 1.00f);
    // A few values
    char searchStringBuffer[128] = {'\0'};
    bool logAutoScroll = true;
    // Wait for process to be attached to
    while (!target.isAttached());
    // Main loop
    while (!glfwWindowShouldClose(window) && target.isAttached()) {
        // Poll events
        glfwPollEvents();
        // Start frame
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();
        // Create main window
        {
            using namespace ImGui;
            Begin("Tracer");
            // Contents
            if (breakpointsReady) {
                TextUnformatted("Function search");
                InputText("Name", searchStringBuffer, IM_ARRAYSIZE(searchStringBuffer));
                // Get search string as C++ string
                std::string_view searchString{searchStringBuffer, strlen(searchStringBuffer)};
                // Show results list if search string is long enough
                if (searchString.size() > 3) {
                    // Show all results
                    BeginChild("Search results", ImVec2(0, GetFontSize() * 20.0f), true);
                    for (auto& [_, breakpointList] : target.getBreakpoints()) {
                        for (auto& breakpoint : breakpointList.getVector()) {
                            if (breakpoint->getName().find(searchString) != std::string::npos) {
                                UIBreakpointButton(breakpoint);
                            }
                        }
                    }
                    EndChild();
                }
                // Show list of enabled breakpoints
                TextUnformatted("Enabled Breakpoints");
                BeginChild("Enabled Breakpoints", ImVec2(0, GetFontSize() * 20.0f), true);
                for (auto& [_, breakpointList] : target.getBreakpoints()) {
                    for (auto& breakpoint : breakpointList.getVector()) {
                        if (breakpoint->isActive()) {
                            UIBreakpointButton(breakpoint);
                        }
                    }
                }
                EndChild();
                // Show tracer log
                TextUnformatted("Tracer log");
                BeginChild("Tracer log", ImVec2(0, GetFontSize() * 20.0f), true);
                TextUnformatted(tracerLog.c_str());
                if (logAutoScroll && ImGui::GetScrollY() >= ImGui::GetScrollMaxY()) {
                    SetScrollHereY(1.0f);
                }
                EndChild();
                // Tracer log controls
                if (Button("Clear")) {
                    tracerLog.clear();
                }
                SameLine();
                Checkbox("Autoscroll", &logAutoScroll);
                // Plugins
                for (auto& plugin : plugins) {
                    if (plugin.isLoaded()) {
                        auto cb = plugin->get<void()>("onUIRender");
                        if (cb) {
                            Begin(plugin->getName().data());
                            try {
                                cb();
                            } catch (std::exception& e) {
                                auto msg = fmt::format("Error executing UI update for plugin {}: {}", plugin->getName(), e.what());
                                logEvent(msg);
                                logger.log(Loglevel::error, msg);
                            }
                            End();
                        }
                    }
                }
            } else {
                TextUnformatted("Breakpoints are not yet ready.");
                TextUnformatted(game.getInitialFunctionTriggerInstructions().data());
            }
            // Window end
            End();
        }
        if (breakpointsReady) {
            // Create plugins window
            using namespace ImGui;
            Begin("Plugins");
            // List all known plugins
            for (auto& plugin : plugins) {
                bool isLoaded = plugin.isLoaded();
                if (Checkbox(plugin.fpath.filename().string().c_str(), &isLoaded)) {
                    try {
                        if (isLoaded) {
                            plugin.load(*this);
                        } else {
                            plugin.unload();
                        }
                    } catch (std::exception& e) {
                        auto msg = fmt::format("Error {}loading plugin: {}", isLoaded?"":"un", e.what());
                        logEvent(msg);
                        logger.log(Loglevel::error, msg);
                    }
                }
            }
            // Window end
            End();
        }
        // Render
        ImGui::Render();
        int display_w, display_h;
        glfwGetFramebufferSize(window, &display_w, &display_h);
        glViewport(0, 0, display_w, display_h);
        glClearColor(clear_color.x * clear_color.w, clear_color.y * clear_color.w, clear_color.z * clear_color.w, clear_color.w);
        glClear(GL_COLOR_BUFFER_BIT);
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
        glfwSwapBuffers(window);
    }
    // Cleanup
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();
    glfwDestroyWindow(window);
    glfwTerminate();
    // Stop main thread
    addToActionQueue([this] () {
        target.detach();
    });
}


Capstone::Capstone(QuickDebug::Target& target, QuickDebug::Addr addr, size_t size) {
    // Initialize capstone
    cs_err err;
    err = cs_open(CS_ARCH_X86, CS_MODE_64, &handle);
    if (err) {
        throw std::runtime_error("Capstone has failed to open");
    }
    cs_option(handle, CS_OPT_DETAIL, CS_OPT_ON);
    // Read instructions
    auto lbtnNetUseAddr = addr;
    std::vector<uint8_t> lbtnNetUseCode(size);
    target.readBuffer(lbtnNetUseAddr, lbtnNetUseCode.data(), lbtnNetUseCode.size());
    // Disassemble
    instr_count = cs_disasm(handle, lbtnNetUseCode.data(), lbtnNetUseCode.size()-1, lbtnNetUseAddr, 0, &instrs);
}
Capstone::~Capstone() {
    cs_close(&handle);
}
}
