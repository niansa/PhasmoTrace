#include "PhasmoTrace/Tracer.hpp"
#include "PhasmoTrace/Game/Phasmophobia_il2cpp.h"
#include "PhasmoTrace/magic_enum.hpp"
#include "QuickDebug.hpp"
#include "imgui.h"

#include <string>
#include <string_view>
#include <unordered_map>
#include <optional>
#include <fstream>
#include <fmt/format.h>



extern "C" {
// Basic declarations
struct Value {
    int lastKnown;
    std::optional<int> override;
};

// Basic definitions
std::string_view PT_pluginName = "Phasmophobia save hacking tool",
                 PT_pluginVersion = "V1.0",
                 PT_pluginDescription = "Allows you to fully customize your save";

// Globals
PhasmoTrace::Tracer *tracer;
std::string settings_fmt;
bool dumped = false;

// Settings
std::unordered_map<std::string, Value> pairs;

// Callbacks
void init() noexcept {
    tracer->activateBreakpointWithName("ES3$$Save<int>");
    tracer->activateBreakpointWithName("ES3$$Load<int>");
    tracer->activateAllBreakpointsWithName("ES3Reader$$Create");
}

void onBreakpoint(QuickDebug::BreakpointList& brkl) {
    // Handle breakpoints
    auto brk = brkl.getVector()[0];
    if (brk->getName() == "ES3$$Save<int>") {
        // Get registers
        const auto& regs = tracer->getTarget().getRegisters();
        // Get key name
        auto name = tracer->getStrAt(regs.rcx);
        // Check if key is known
        auto pair = pairs.find(name);
        if (pair != pairs.end()) { // Key is known
            // Check if value is to be overridden
            if (pair->second.override.has_value()) {
                // Override value
                auto newRegs = regs;
                newRegs.rdx = pair->second.override.value();
                tracer->getTarget().setRegisters(newRegs);
            }
            // Update last known value
            pair->second.lastKnown = regs.rdx;
        } else { // Key is unknown
            // Make it known
            pairs[name] = {static_cast<int>(regs.rdx)};
        }
    } else if (brk->getName() == "ES3$$Load<int>") {
        // Get registers
        const auto& regs = tracer->getTarget().getRegisters();
        // Get key name
        auto name = tracer->getStrAt(regs.rcx);
        // Check if key is known
        auto pair = pairs.find(name);
        if (pair != pairs.end()) { // Key is known
            // Check that value is to be overridden
            if (pair->second.override.has_value()) {
                // Override value
                auto newRegs = regs;
                newRegs.rax = pair->second.override.value();
                tracer->getTarget().setRegisters(newRegs);
                // Return
                tracer->getTarget().writeByte(brk->getAddr(), QuickDebug::SimpleInstructions::ret);
            }
        } else { // Key is unknown
            // Make it known
            pairs[name] = {static_cast<int>(regs.rdx)};
        }
    } else if (settings_fmt.empty() && brk->getName() == "ES3Reader$$Create") { // TODO: Make sure the correct overload is being called
        // Dump settings
        const auto& regs = tracer->getTarget().getRegisters();
        auto settings = tracer->getTarget().readObject<Il2Cpp::Object<Il2Cpp::ES3::Settings>>(regs.rcx).fields;
        settings_fmt = fmt::format("ES3 Settings:\n"
                                        " - location: {}\n"
                                        " - path: {}\n"
                                        " - encryptionType: {}\n"
                                        " - compressionType: {}\n"
                                        " - encryptionPassword: {}\n"
                                        " - directory: {}\n"
                                        " - format: {}\n"
                                        " - prettyPrint: {}\n"
                                        " - bufferSize: {}\n"
                                        "   ...",
                                        settings.location,
                                        tracer->getStrAt(settings.path),
                                        settings.encryptionType,
                                        settings.compressionType,
                                        tracer->getStrAt(settings.encryptionPassword),
                                        settings.directory,
                                        settings.format,
                                        settings.prettyPrint,
                                        settings.bufferSize);
    }
}

void onUIRender() noexcept {
    using namespace ImGui;
    for (auto& [key, value] : pairs) {
        PushID(key.c_str());
        Text("- %s: %i", key.c_str(), value.lastKnown);
        SameLine();
        bool isOverridden = value.override.has_value();
        if (Checkbox("Override", &isOverridden)) {
            if (isOverridden) {
                value.override = value.lastKnown;
            } else {
                value.override.reset();
            }
        }
        if (isOverridden) {
            SameLine();
            InputInt("Value", &value.override.value());
        }
        PopID();
    }
    Separator();
    if (!settings_fmt.empty()) {
        TextUnformatted(settings_fmt.c_str());
        if (dumped) {
            TextUnformatted("Dumped to es3settings.txt");
        } else if (Button("Dump")) {
            std::ofstream dump("es3settings.txt");
            dump << settings_fmt;
            dumped = true;
        }
    } else {
        TextUnformatted("Click singleplayer in main menu to see more...");
    }
}
}
