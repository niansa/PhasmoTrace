#include "PhasmoTrace/Tracer.hpp"
#include "PhasmoTrace/Game/Phasmophobia_il2cpp.h"
#include "QuickDebug.hpp"
#include "imgui.h"

#include <string_view>
#include <string>
#include <vector>



// Basic definitions
extern "C" {
std::string_view PT_pluginName = "Phasmophobia Lobby List",
                 PT_pluginVersion = "V1.0",
                 PT_pluginDescription = "A unfiltered Lobby List for Phasmophobia.";

// Globals
PhasmoTrace::Tracer *tracer;
}
std::vector<std::string> lobbyList;

// Settings
//...

// Callbacks
extern "C" {
void init() noexcept {
    tracer->activateBreakpointWithName("LobbyManager$$OnRoomListUpdate");
}

void onBreakpoint(QuickDebug::BreakpointList& brkl) {
    // Handle breakpoints
    auto brk = brkl.getVector()[0];
    if (brk->getName() == "LobbyManager$$OnRoomListUpdate") {
        // Get list object
        auto list = tracer->getTarget().readObject<Il2Cpp::Object<Il2Cpp::System::Collections::Generic::List>>(tracer->getTarget().getRegisters().rdx);
        // Get array object
        auto arrayBuf = list.fields._items;
        auto array = tracer->getTarget().readObject<Il2Cpp::BuiltIn::Il2CppArray>(arrayBuf);
        // Iterate though array
        auto array_first_item_off = reinterpret_cast<QuickDebug::Addr>(&array.first_item) - reinterpret_cast<QuickDebug::Addr>(&array);
        for (unsigned it = 0; it != list.fields._size; it++) {
            // Get item buffer
            auto lobbyBuf = tracer->getTarget().readObject<QuickDebug::Addr>(arrayBuf+array_first_item_off+(it*sizeof(QuickDebug::Addr)));
            // Get item
            auto lobby = tracer->getTarget().readObject<Il2Cpp::Object<Il2Cpp::Photon::Realtime::RoomInfo>>(lobbyBuf);
            // Add name
            lobbyList.push_back(tracer->getStrAt(lobby.fields.name));
        }
    }
}

void onUIRender() noexcept {
    using namespace ImGui;
    // Render UI
    if (Button("Clear")) {
        lobbyList.clear();
    }
    Separator();
    for (const auto& lobbyName : lobbyList) {
        Text("%s", lobbyName.c_str());
    }
}
}
