#include "PhasmoTrace/Tracer.hpp"
#include "PhasmoTrace/Game/Phasmophobia_il2cpp.h"
#include "PhasmoTrace/magic_enum.hpp"
#include "PhasmoTrace/Timer.hpp"
#include "QuickDebug.hpp"
#include "imgui.h"

#include <string>
#include <string_view>
#include <vector>
#include <stack>
#include <exception>
#include <queue>
#include <functional>



extern "C" {
// Basic definitions
std::string_view PT_pluginName = "Simple Phasmophobia anti-troll",
                 PT_pluginVersion = "WIP",
                 PT_pluginDescription = "This plugin allows you to manage trolls more easily.";

// Globals
PhasmoTrace::Tracer *tracer;
}

// Data containers
static std::vector<struct PhotonPlayer> players;
static std::vector<struct GamePlayer> unassocPlayers;
static QuickDebug::Addr serverManager, photonRoom = 0;
static bool inGame = false;

// Basic declarations
struct GamePlayer {
    QuickDebug::Addr addr = 0;
    bool hasCachedObject = false;

    operator bool() const {
        return addr;
    }
};

struct PhotonPlayer {
    QuickDebug::Addr addr = 0;
    GamePlayer playerObject;
    std::string nickName;
    int actorNumber;
    std::string playerID = "Loading...";
    unsigned index;

    PhasmoTrace::Timer creationTimer;

    bool punished = false, pardoned = false;

    operator bool() const {
        return addr;
    }

    bool punish() {
        bool fres = false;;
        // Try to force disconnect (unreliable)
        tracer->addToOnUpdateQueue([this] () {
            tracer->call("Photon.Pun.PhotonNetwork$$CloseConnection", {addr});
        });
        // Try "special treatmend"
        if (!inGame) {
            // Regular kick
            tracer->addToOnUpdateQueue([this] () {
                tracer->call("ServerManager$$KickPlayer", {serverManager, index, tracer->getTarget().getRegisters().rdx});
            });
            fres = true;
        } else if (playerObject) {
            // Deadzone jail
            for (const std::string_view fnc : {"Player$$StartKillingPlayer", "Player$$StartKillingPlayer", "Player$$KillPlayer"}) {
                tracer->addToOnUpdateQueue([this, fnc] () {
                    tracer->call(fnc, {playerObject.addr});
                });
            }
            punished = true;
            pardoned = false;
            fres = true;
        }
        // Flush
        tracer->addToOnUpdateQueue([this] () {
            tracer->call("Photon.Pun.PhotonNetwork$$SendAllOutgoingCommands", {tracer->getTarget().getRegisters().rdx});
        });
        return fres;
    }

    void pardon() {
        if (playerObject) {
            tracer->addToOnUpdateQueue([this] () {
                tracer->call("Player$$RevivePlayer", {playerObject.addr});
            });
            punished = false;
            pardoned = true;
        }
    }
};

// Settings
static bool forceAllowTruckClose = false;

// Helpers
PhotonPlayer *getPhotonPlayerByAddr(QuickDebug::Addr addr) noexcept {
    unsigned index = 1;
    for (auto& player : players) {
        if (player.addr == addr) {
            player.index = index;
            return &player;
        }
        index++;
    }
    return nullptr;
}
PhotonPlayer *getPhotonPlayerByActorNumber(int actorNumber) noexcept {
    unsigned index = 1;
    for (auto& player : players) {
        if (player.actorNumber == actorNumber) {
            player.index = index;
            return &player;
        }
        index++;
    }
    return nullptr;
}
PhotonPlayer *getPhotonPlayerByPlayerAddr(QuickDebug::Addr addr) noexcept {
    unsigned index = 1;
    for (auto& player : players) {
        if (player.playerObject.addr == addr) {
            player.index = index;
            return &player;
        }
        index++;
    }
    return nullptr;
}

PhotonPlayer *getHost() {
    // The player with the lowest actor number is always the host
    PhotonPlayer *fres = nullptr;
    unsigned lowest = -1;
    for (auto& player : players) {
        if (player.actorNumber < lowest) {
            lowest = player.actorNumber;
            fres = &player;
        }
    }
    return fres;
}

void removePhotonPlayerByAddr(QuickDebug::Addr addr) noexcept {
    for (auto it = players.begin(); it != players.end(); it++) {
        if (it->addr == addr) {
            players.erase(it);
            return;
        }
    }
}
void removePhotonPlayerByActorNumber(int actorNumber) noexcept {
    for (auto it = players.begin(); it != players.end(); it++) {
         if (it->actorNumber == actorNumber) {
            players.erase(it);
            return;
        }
    }
}

decltype(unassocPlayers)::iterator getUnassocPlayer(QuickDebug::Addr addr) {
    for (auto it = unassocPlayers.begin(); it != unassocPlayers.end(); it++) {
        if (it->addr == addr) {
            return it;
        }
    }
    return unassocPlayers.end();
}

GamePlayer *getPlayer(QuickDebug::Addr addr) {
    // Try unassociated player
    auto unassocPlayer = getUnassocPlayer(addr);
    if (unassocPlayer != unassocPlayers.end()) {
        return std::addressof(*unassocPlayer);
    }
    // Try associated player
    auto photonPlayer = getPhotonPlayerByAddr(addr);
    if (photonPlayer && photonPlayer->playerObject) {
        return &photonPlayer->playerObject;
    }
    // Give up
    return nullptr;
}

GamePlayer *findPlayerIf(const std::function<bool(GamePlayer&)>& cb) {
    for (auto& photonPlayer : players) {
        if (photonPlayer.playerObject && cb(photonPlayer.playerObject)) {
            return &photonPlayer.playerObject;
            break;
        }
    }
    for (auto& unassocPlayer : unassocPlayers) {
        if (cb(unassocPlayer)) {
            return &unassocPlayer;
        }
    }
    return nullptr;
}


// Callbacks
extern "C" {
void init() noexcept {
    // Add the breakpoints we need
    tracer->addToActionQueue([] () {
        for (const auto& name : {"ServerManager$$Start",
                                 "Player$$.ctor",
                                 "Photon.Realtime.Player$$.ctor",
                                 "Photon.Realtime.Room$$RemovePlayer",
                                 "Photon.Pun.PhotonNetwork$$LeftRoomCleanup",
                                 "Photon.Pun.PhotonNetwork$$LoadLevel",
                                 "Photon.Realtime.Player$$set_NickName",
                                 "GameController$$GetPlayerFromPhotonPlayer",
                                 "ServerManager$$StartGame",
                                 "ExitLevel$$Exit"}) {
            for (auto& brk : tracer->getTarget().getBreakpointsByName(name)) {
                tracer->getTarget().activateBreakpoint(brk);
            }
        }
    });
}

void onBreakpoint(QuickDebug::BreakpointList& brkl) {
    auto& brk = brkl.getVector()[0];
    // Update pointers
    if (!photonRoom) {
        if (brk->getName().find("Photon.Realtime.Room$$") == 0) {
            photonRoom = tracer->getTarget().getRegisters().rcx;
            inGame = false;
        }
    }
    if (brk->getName() == "ServerManager$$Start") {
        serverManager = tracer->getTarget().getRegisters().rcx;
        inGame = false;
    }
    // Detect game start/end
    else if (brk->getName() == "Photon.Pun.PhotonNetwork$$LoadLevel") {
        inGame = true;
        unassocPlayers.clear();
        // Reset all player object addrs and some trust stuff
        for (auto& photonPlayer : players) {
            photonPlayer.playerObject.addr = 0;
            photonPlayer.punished = false;
        }
    } else if (brk->getName() == "ExitLevel$$Exit") {
        inGame = false;
        unassocPlayers.clear();
        // Increase all players game counts
        for (auto& player : players) {
            player.punished = false;
        }
    }
    // Keep track of player list
    else if (brk->getName() == "Player$$.ctor") { // Player object was created
        if (inGame && photonRoom) {
            unassocPlayers.push_back(GamePlayer{tracer->getTarget().getRegisters().rcx});
        }
    } else if (brk->getName() == "Photon.Realtime.Player$$.ctor") { // Other player has joined
        // Get information about the new player as follows:
        // void Photon_Realtime_Player___ctor (Photon_Realtime_Player_o* __this, System_String_o* nickName, int32_t actorNumber, bool isLocal, ...);
        //                                                               ^ rcx                    ^ rdx             ^ r8              ^ r9
        //                                                                                         always empty                        always false
        const auto& regs = tracer->getTarget().getRegisters();
        PhotonPlayer newPlayer;
        newPlayer.addr = regs.rcx;
        newPlayer.actorNumber = regs.r8;
        // Get possibly existing player
        auto existingPlayer = getPhotonPlayerByAddr(newPlayer.addr);
        if (existingPlayer) { // Update player
            *existingPlayer = std::move(newPlayer);
        } else {
            // Add player to list
            players.push_back(std::move(newPlayer));
        }
    } else if (brk->getName() == "GameController$$GetPlayerFromPhotonPlayer") { // Requesting player from photon player
        auto& target = tracer->getTarget();
        auto photonPlayer = getPhotonPlayerByAddr(target.getRegisters().rdx);
        if (photonPlayer) {
            tracer->continueUntilReturn();
            // Try to associate unassociated player
            auto unassocPlayer = getUnassocPlayer(target.getRegisters().rax);
            if (unassocPlayer != unassocPlayers.end()) {
                photonPlayer->playerObject = std::move(*unassocPlayer);
                unassocPlayers.erase(unassocPlayer);
            } else {
                photonPlayer->playerObject.addr = target.getRegisters().rax;
            }
        }
        // There MUST be a faster way to get this pointer!?!?!?
    } else if (brk->getName() == "Photon.Realtime.Room$$RemovePlayer") { // Other player has left
        const auto& regs = tracer->getTarget().getRegisters();
        // Get signature
        const auto& signature = std::any_cast<const PhasmoTrace::Function*>(brk->getData())->signature;
        // Act accordingly
        if (signature == "void Photon_Realtime_Room__RemovePlayer (Photon_Realtime_Room_o* __this, Photon_Realtime_Player_o* player, const MethodInfo* method);") {
            //                                                                             ^ rcx                             ^ rdx
            removePhotonPlayerByAddr(regs.rdx);
        } else if (signature == "void Photon_Realtime_Room__RemovePlayer (Photon_Realtime_Room_o* __this, int32_t id, const MethodInfo* method);") {
            //                                                                                    ^ rcx           ^ rdx
            removePhotonPlayerByActorNumber(regs.rdx);
        }
    } else if (brk->getName() == "Photon.Pun.PhotonNetwork$$LeftRoomCleanup") { // Room is getting cleaned up
        // Clean up
        players.clear();
        unassocPlayers.clear();
        photonRoom = 0;
        inGame = false;
    } else if (brk->getName() == "Photon.Realtime.Player$$set_NickName") { // Player nickname set
        const auto& regs = tracer->getTarget().getRegisters();
        auto player = getPhotonPlayerByAddr(regs.rcx);
        if (player) {
            // Check that player does not already have a name
            if (player->nickName.empty()) {
                try {
                    player->nickName = tracer->getStrAt(regs.rdx);
                } catch (std::exception&) {}

            }
        }
    }
    // Force truck to allow starting
    else if (forceAllowTruckClose && brk->getName() == "ExitLevel$$ThereAreAlivePlayersOutsideTheTruck") {
        auto regs = tracer->getTarget().getRegisters();
        regs.rax = false;
        tracer->getTarget().setRegisters(regs);
        tracer->getTarget().writeByte(brk->getAddr(), QuickDebug::SimpleInstructions::ret);
    }
}

void onUIRender() noexcept {
    using namespace ImGui;
    TextUnformatted("WARNING: You need to be host for this plugin to function correctly.");
    // Update stuff
    for (auto& player : players) {
        if (player.playerID == "Loading...") {
            if (player.creationTimer.get<std::chrono::seconds>() > 1) {
                player.playerID = "Unknown";
                tracer->addToActionQueue([&player] () {
                    auto playerObj = tracer->getTarget().readObject<Il2Cpp::Object<Il2Cpp::Photon::Realtime::Player>>(player.addr);
                    player.playerID = tracer->getStrAt(playerObj.fields.userID);
                });
            }
        }
    }
    // Overview
    Separator();
    TextUnformatted("Overview");
    Text(" - In room: %s", photonRoom ? "yes" : "no");
    Text(" - In game: %s", inGame ? "yes" : "no");
    if (photonRoom) {
        // Player list
        Separator();
        TextUnformatted("Players");
        BeginChild("Player list", ImVec2(0, GetFontSize()*11.0f), true);
        {
            unsigned idx = 2;
            for (auto& player : players) {
                player.index = idx++;
                PushID(std::to_string(player.addr).c_str()); // Slow but reliable
                if (player.punished) {
                    if (Button("Pardon")) {
                        player.pardon();
                    }
                } else {
                    BeginDisabled(!player.playerObject && inGame);
                    if (Button("Punish")) {
                        player.punish();
                    }
                    EndDisabled();
                }
                Text("(%p/%p) %i: %s (Steam: %s)", reinterpret_cast<void*>(player.addr), reinterpret_cast<void*>(player.playerObject.addr), player.actorNumber, player.nickName.c_str(), player.playerID.c_str());
                PopID();
                Separator();
            }
        }
        EndChild();
        // Unassociated player list
        if (!unassocPlayers.empty() && inGame) {
            Separator();
            TextUnformatted("Unassociated players");
            BeginChild("Unassociated player list", ImVec2(0, GetFontSize()*8.0f), true);
            {
                unsigned idx = 2;
                for (auto& unassocPlayer : unassocPlayers) {
                    PushID(std::to_string(unassocPlayer.addr).c_str()); // Slow but reliable
                    Text("%p", reinterpret_cast<void*>(unassocPlayer.addr));
                    PopID();
                }
            }
            EndChild();
       }
    }
}

std::string *insideHTTPOverview() {
    if (photonRoom) {
        std::ostringstream os;
        os << "<h2>Current multiplayer lobby</h2>"
              "<p>Currently " << (inGame ? "" : "not") << " in game</p>"
              "<p>Currently connected players: ";
        if (players.empty()) {
            os << "none";
        } else {
            for (const auto& player : players) {
                os << "<u>" << player.nickName << "</u> ";
            }
        }
        os << "</p>";
        return new auto(os.str());
    } else {
        return nullptr;
    }
}
}
