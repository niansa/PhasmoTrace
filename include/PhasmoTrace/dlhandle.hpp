#ifndef __WIN32
#include <string>
#include <exception>
#include <dlfcn.h>


class Dlhandle {
    void *chandle;
    unsigned *user_counter;

public:
    class Exception : public std::exception {
        std::string errmsg;
    public:
        Exception(std::string errmsg) {
            this->errmsg = errmsg;
        }
        virtual const char* what() const throw() {
            return errmsg.c_str();
        }
    };

    Dlhandle(std::string fpath, int flags = RTLD_LAZY) {
        chandle = dlopen(fpath.c_str(), flags);
        if (!chandle) {
            throw Exception("dlopen(): "+fpath);
        }
        user_counter = new unsigned(1);
    }
    Dlhandle(const Dlhandle& o) {
        chandle = o.chandle;
        user_counter = o.user_counter;
        (*user_counter)++;
    }
    ~Dlhandle() {
        (*user_counter)--;
        if (*user_counter == 0) {
            dlclose(chandle);
            delete user_counter;
        }
    }

    template<typename T>
    T* get(const std::string& fname) {
        dlerror(); // Clear error
        auto fres = reinterpret_cast<T*>(dlsym(chandle, fname.c_str()));
        return (dlerror()==NULL)?fres:nullptr;
    }
    auto get_fnc(const std::string& fname) {
        return get<void*(...)>(fname);
    }
};
#else
#include <string>
#include <exception>
#include <libloaderapi.h>



class Dlhandle {
    HMODULE chandle;
    unsigned *user_counter;

public:
    class Exception : public std::exception {
        std::string errmsg;
    public:
        Exception(std::string errmsg) {
            this->errmsg = errmsg;
        }
        virtual const char* what() const throw() {
            return errmsg.c_str();
        }
    };

    Dlhandle(std::string fpath) {
        chandle = LoadLibraryA(fpath.c_str());
        if (!chandle) {
            throw Exception("dlopen(): "+fpath);
        }
        user_counter = new unsigned(1);
    }
    Dlhandle(const Dlhandle& o) {
        chandle = o.chandle;
        user_counter = o.user_counter;
        (*user_counter)++;
    }
    ~Dlhandle() {
        (*user_counter)--;
        if (*user_counter == 0) {
            FreeLibrary(chandle);
            delete user_counter;
        }
    }

    template<typename T>
    T* get(const std::string& fname) {
        return reinterpret_cast<T*>(GetProcAddress(chandle, fname.c_str()));
    }
    auto get_fnc(const std::string& fname) {
        return get<void*(...)>(fname);
    }
};
#endif
