#ifndef __WIN32
#include "PhasmoTrace/Tracer.hpp"
#include "crow.h"

#include <sstream>



namespace PhasmoTrace {
void Tracer::runHTTP() {
    crow::SimpleApp app;

    CROW_ROUTE(app, "/")([this, &app](){
        // Stop app if target is no longer attached
        if (!target.isAttached()) {
            app.stop();
            crow::response resp;
            resp.code = 410;
            return resp;
        }
        // Deliver overview page
        std::ostringstream os;
        os << "<meta http-equiv='refresh' content='5'>"
              "<h1>Phasmophobia Website control</h1><br/>";
        for (auto& plugin : plugins) {
            if (plugin.isLoaded()) {
                auto cb = plugin->get<std::string*()>("insideHTTPOverview");
                if (cb) {
                    auto str = cb();
                    if (str) {
                        os << *str;
                        delete str;
                    }
                }
            }
        }
        crow::response resp;
        resp.add_header("Content-Type", "text/html");
        resp.body = os.str();
        return resp;
    });

    CROW_ROUTE(app, "/<string>/<int>")([this](const std::string& trigger, int param){
        for (auto& plugin : plugins) {
            if (plugin.isLoaded()) {
                auto cb = plugin->get<void(const std::string&, int)>("onHTTPTrigger");
                if (cb) {
                    cb(trigger, param);
                }
            }
        }
        crow::response resp;
        resp.redirect("/");
        return resp;
    });

    app.port(8088).multithreaded().run();
}
}
#endif
