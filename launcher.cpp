#include <iostream>
#include <string>
#include <fstream>
#include <filesystem>
#include <chrono>
#include <thread>
#include <windows.h>
#include <tlhelp32.h>
#include <psapi.h>


DWORD GetPIDByName(PCSTR name) {
    DWORD pid = 0;

    // Create toolhelp snapshot.
    HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
    PROCESSENTRY32 process;
    ZeroMemory(&process, sizeof(process));
    process.dwSize = sizeof(process);

    // Walkthrough all processes.
    if (Process32First(snapshot, &process)) {
        do {
            // Compare process.szExeFile based on format of name, i.e., trim file path
            // trim .exe if necessary, etc.
            if (std::string(process.szExeFile) == std::string(name)) {
               pid = process.th32ProcessID;
               break;
            }
        } while (Process32Next(snapshot, &process));
    }

    CloseHandle(snapshot);

    return pid;
}

int main() {
    // Start the game
    SHELLEXECUTEINFOA ShExecInfo;
    ShExecInfo.cbSize = sizeof(SHELLEXECUTEINFO);
    ShExecInfo.fMask = SEE_MASK_NOCLOSEPROCESS;
    ShExecInfo.hwnd = nullptr;
    ShExecInfo.lpVerb = "open";
    ShExecInfo.lpFile = "steam://rungameid/739630";
    ShExecInfo.lpParameters = "";
    ShExecInfo.lpDirectory = nullptr;
    ShExecInfo.nShow = SW_SHOW;
    ShExecInfo.hInstApp = nullptr;
    ShellExecuteExA(&ShExecInfo);

    // Delay
    std::cout << "Locating game..." << std::endl;
    std::this_thread::sleep_for(std::chrono::seconds(20));

    // Get PID
    auto pid = GetPIDByName("Phasmophobia.exe");
    if (pid == 0) {
        MessageBoxA(nullptr, "Could not locate running game", "PhasmoTrace Launcher", MB_OK);
        std::cout << "Launcher has failed." << std::endl;
        return 1;
    }

    // Open process
    /*auto proc = OpenProcess(PROCESS_ALL_ACCESS, TRUE, pid);
    if (!proc) {
        MessageBoxA(nullptr, "Could not access process - try running me as admin", "PhasmoTrace Launcher", MB_OK);
        std::cout << "Launcher has failed." << std::endl;
        return 2;
    }

    // Get file path
    char *executablePath_c = new char[MAX_PATH*4];
    executablePath_c[0] = '\0';
    if (!GetModuleFileNameExA(proc, NULL, executablePath_c, MAX_PATH*4)) {
        MessageBoxA(nullptr, "Could not locate game file", "PhasmoTrace Launcher", MB_OK);
        std::cout << "Launcher has failed." << std::endl;
        return 3;
    }
    std::filesystem::path executablePath = executablePath_c;
    delete []executablePath_c;

    // Get GameAssembly path
    auto gameAssemblyPath = executablePath.parent_path() / "GameAssembly.dll";

    // Check if gameAssembly is patched  TODO: Fixme
    {
        char magic[4];

        // Open file
        std::fstream gameAssembly(gameAssemblyPath, std::ios::openmode::_S_in | std::ios::openmode::_S_bin);
        if (!gameAssembly) {
            MessageBoxA(nullptr, "Could not open GameAssembly in read mode", "PhasmoTrace Launcher", MB_OK);
            std::cout << "Launcher has failed." << std::endl;
            return 4;
        }

        // Seek to the end before magic
        gameAssembly.seekg(std::filesystem::file_size(gameAssemblyPath) - sizeof(magic));

        // Read last 4 bytes
        gameAssembly.read(magic, sizeof(magic));

        // Make sure magic is correct
        if (magic[0] != 'P' || magic[0] != 'H' || magic[0] != 'S' || magic[0] != 'T') {
            std::cout << "Preparing GameAssembly..." << std::endl;

            // Kill process
            TerminateProcess(proc, 1);
            CloseHandle(proc);

            // Reopen in write mode
            gameAssembly = std::fstream(gameAssemblyPath, std::ios::openmode::_S_out | std::ios::openmode::_S_bin);
            if (!gameAssembly) {
                MessageBoxA(nullptr, "Could not open GameAssembly in write mode - try running me as admin", "PhasmoTrace Patcher", MB_OK);
                std::cout << "Patcher has failed." << std::endl;
                return 5;
            }

            // Patch GameAssembly
            //TODO: Implement
            // Fallback
            MessageBoxA(nullptr, "Automatic patching is not yst implemented. DIY!", "PhasmoTrace Patcher", MB_OK);
            return 6;
        }
    }*/

    // Start the tracer
    const std::string args = "run "+std::to_string(pid)+" script.json";
    ShExecInfo.lpFile = "phasmotrace.exe";
    ShExecInfo.lpParameters = args.c_str();
    ShellExecuteExA(&ShExecInfo);

    // Wait for tracer to exit
    WaitForSingleObject(ShExecInfo.hProcess, INFINITE);

    // End
    std::cout << "Tracer has exited." << std::endl;
    getchar();

    return 0;
}
