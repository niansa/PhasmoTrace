#ifndef _TRACERCAPSTONE_HPP
#define _TRACERCAPSTONE_HPP
#include "QuickDebug.hpp"
#include "capstone/capstone.h"

#include <stdint.h>



namespace PhasmoTrace {
struct Capstone {
    csh handle;
    cs_insn *instrs = nullptr;
    size_t instr_count;

    Capstone(QuickDebug::Target& target, QuickDebug::Addr addr, size_t size);
    ~Capstone();
};
}
#endif
