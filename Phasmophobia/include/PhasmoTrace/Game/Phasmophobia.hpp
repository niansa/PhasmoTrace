#ifndef _PHASMOPHOBIA_HPP
#define _PHASMOPHOBIA_HPP
#include "PhasmoTrace/Tracer.hpp"
#include "PhasmoTrace/Game/Phasmophobia_il2cpp.h"
#include "QuickDebug.hpp"

#include <string>
#include <string_view>
#include <vector>
#include <variant>



namespace Phasmophobia {
enum class GhostState {
    idle = 0,
    wander = 1,
    hunt = 2,
    favouriteRoom = 3,
    light = 4,
    door = 5,
    throwing = 6,
    fusebox = 7,
    appear = 8,
    doorKnock = 9,
    windowKnock = 10,
    carAlarm = 11,
    radio = 12,
    flicker = 13,
    normalEvent = 14,
    randomEvent = 15,
    ability = 16,
    mannequin = 17,
    teleportObject = 18,
    summon = 19,
    sing = 20
};

struct Item {
    static constexpr auto dynamicInstanceCount = 0;

    std::string name;
    std::string friendlyName;
    std::string knownTriggerFunctionName;
    std::string startFunctionName;
    QuickDebug::Addr trigger = 0;
    std::vector<QuickDebug::Addr> instances;
    decltype(instances)::iterator instanceIterator;
    bool instanceRing;
    unsigned triggerNo;
    bool disturbed = false;

    Item(std::string_view name, std::string_view friendlyName, decltype(instances)::size_type instanceCount = dynamicInstanceCount, std::variant<int, const char *> knownTriggerNoOrName = {}, std::string_view startFunctionName = "Start");
    Item(const Item& o);
    Item(Item&& o);

    bool isUsable() const {
        return hasInstances() && trigger;
    }

    QuickDebug::Addr& nextInstance();
    void clearInstances();
    bool hasInstances() const;

    void findTrigger();
    void use(unsigned instance = 0) const;
};


void initGameBase(PhasmoTrace::Tracer *);
}
#endif
