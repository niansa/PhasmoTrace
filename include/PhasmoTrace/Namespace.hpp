#ifndef NAMESPACE_HPP
#define NAMESPACE_HPP
namespace QuickDebug {}
namespace QLog {}

namespace PhasmoTrace {
using namespace QuickDebug;
using namespace QLog;
}
#endif // NAMESPACE_HPP
