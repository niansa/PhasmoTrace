#include "PhasmoTrace/Tracer.hpp"

#include <iostream>
#include <fstream>
#include <string_view>
#include <thread>


void print_usage(const char *executable) {
    std::cerr << "Wrong usage" << std::endl;
    std::cout << "Usage: " << executable << " run <game object> <path to gameassembly> <path to script.json> <pid>" << std::endl
              << "       " << executable << " prepare <game object> <path to gameassembly> <path to script.json>" << std::endl;
}

int main(int argc, char **argv) {
    // Check args
    if (argc < 5) {
        print_usage(argv[0]);
        return 1;
    }
    // Create tracer and load script.json
    PhasmoTrace::Tracer tracer(argv[2]);
    tracer.loadScript(argv[4]);
    std::string_view mode = argv[1];
    if (mode == "run") {
        // Kick in execution of all threads
        std::thread ui([&](){tracer.runUI();});
#       ifndef __WIN32
        std::thread http([&](){tracer.runHTTP();});
#       endif
        tracer.run(std::atol(argv[5]), argv[3]);
        ui.join();
#       ifndef __WIN32
        http.join();
#       endif
    } else if (mode == "prepare") {
#       ifndef __WIN32
        // Patch gameassebly
        tracer.patch_gameassembly(argv[3]);
#       else
        std::cerr << "Error: Patching is not yet supported on Windows." << std::endl;
#       endif
    } else {
        print_usage(argv[0]);
        return 1;
    }
    return 0;
}
