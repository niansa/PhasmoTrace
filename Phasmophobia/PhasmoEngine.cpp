#include "PhasmoTrace/Tracer.hpp"
#include "PhasmoTrace/magic_enum.hpp"
#include "QuickDebug.hpp"
#include "imgui.h"
#include "PhasmoEngine/Ghost.hpp"

#include <string_view>
#include <sstream>
#include <unordered_map>
#include <optional>
#include <stack>
#include <utility>
#include <algorithm>



extern "C" {
// Enums for some basic stuff
enum class GhostState {
    idle = 0,
    roam = 1,
    hunt = 2,
    wander = 3,
    interaction = 6,
    event = 14,
    ability = 15,
    summon = 19
};

// Basic definitions
std::string_view PT_pluginName = "Phasmoengine",
                 PT_pluginVersion = "V1.0",
                 PT_pluginDescription = "Custom implementation of ghost AI";

// Globals
PhasmoTrace::Tracer *tracer;
bool forceGhostInteractionOnce = false;
std::shared_ptr<QuickDebug::Breakpoint> getAveragePlayerInsanityRetBrk = nullptr;
QuickDebug::Addr lastGameControllerAddr;

// Settings
static std::optional<GhostState> lastKnownGhostState;
static GhostState ghostStateToSet;

// PhasmoEngine
class LevelManager final : public PhasmoEngine::LevelManager {
public:
    unsigned sanity = 100;

    unsigned getTeamSanity() override {
        return sanity;
    }
} levelManager;

class CustomGhost final : public PhasmoEngine::Ghost {
    GhostState convertGhostState(PhasmoEngine::GhostState state) {
        switch (state) {
        case PhasmoEngine::GhostState::local: return GhostState::wander;
        case PhasmoEngine::GhostState::roam: return GhostState::roam;
        case PhasmoEngine::GhostState::reveal: return GhostState::event;
        case PhasmoEngine::GhostState::hunt: return GhostState::hunt;
        }
        return GhostState::wander; // To satisfy clang
    }
    PhasmoEngine::GhostState convertGhostState(GhostState state) {
        switch (state) {
        case GhostState::wander: return PhasmoEngine::GhostState::local;
        case GhostState::roam: return PhasmoEngine::GhostState::roam;
        case GhostState::event: return PhasmoEngine::GhostState::reveal;
        case GhostState::hunt: return PhasmoEngine::GhostState::hunt;
        default: return PhasmoEngine::GhostState::local;
        }
    }

public:
    LevelManager *getLevelManager() override {
        return &levelManager;
    }
    void setState(PhasmoEngine::GhostState state) override {
        ghostStateToSet = convertGhostState(state);
    }
    PhasmoEngine::GhostState getState() override {
        return convertGhostState(ghostStateToSet);
    }
    void tryInteract(PhasmoEngine::InteractionType::Type) override {
        forceGhostInteractionOnce = true;
    }
} ghost;

// Callbacks
void init() noexcept {
    tracer->activateBreakpointWithName("GhostAI$$ChangeState");
    tracer->activateBreakpointWithName("GhostAI$$Update");
    if (!getAveragePlayerInsanityRetBrk) {
        tracer->activateBreakpointWithName("GameController$$GetAveragePlayerInsanity");
    } else {
        tracer->getTarget().activateBreakpoint(getAveragePlayerInsanityRetBrk);
    }
    ghost.autoPrepare();
}

void onBreakpoint(QuickDebug::BreakpointList& brkl) {
    // Handle breakpoints
    auto brk = brkl.getVector()[0];
    if (brk->getName() == "GhostAI$$Update") {
        if (forceGhostInteractionOnce) {
            auto regs = tracer->getTarget().getRegisters();
            regs.rip =  tracer->getTarget().getBreakpointsByName("GhostActivity$$Interact")[0]->getAddr();
            tracer->getTarget().setRegisters(regs);
            forceGhostInteractionOnce = false;
            return;
        }
    }
    if (brk->getName() == "GhostAI$$ChangeState") {
        auto regs = tracer->getTarget().getRegisters();
        if (lastKnownGhostState.has_value()) {
            if (lastKnownGhostState.value() != GhostState::interaction) {
                if (lastKnownGhostState.value() == GhostState::wander) {
                    regs.rdx = magic_enum::enum_integer(ghostStateToSet);
                } else {
                    regs.rdx = magic_enum::enum_integer(GhostState::wander);
                }
                tracer->getTarget().setRegisters(regs);
            }
        }
        lastKnownGhostState = static_cast<GhostState>(regs.rdx);
        return;
    }
    if (!getAveragePlayerInsanityRetBrk && brk->getName() == "GameController$$GetAveragePlayerInsanity") {
        lastGameControllerAddr = tracer->getTarget().getRegisters().rcx;
        tracer->continueUntilReturn();
        // Deactivate this breakpoint
        tracer->getTarget().deactivateBreakpoint(brk);
        // Make a new breakpoint at function return
        getAveragePlayerInsanityRetBrk = brk = tracer->getTarget().addBreakpoint(tracer->getTarget().getRegisters().rip, brk->getName()+"+ret");
        tracer->getTarget().activateBreakpoint(getAveragePlayerInsanityRetBrk);
    }
    if (brk == getAveragePlayerInsanityRetBrk) {
        /*TODO: Fix this, it always leads to 0 sanity
        // Load gamecontroller
        auto gameController = tracer->getTarget().readObject<Il2Cpp::Object<Il2Cpp::Game::GameController>>(lastGameControllerAddr);
        // Sync team sanity
        levelManager.sanity = gameController.teamSanity;
        */
        return;
    }
}

void onUIRender() noexcept {
    // Ghost AI runs in UI thread
    ghost.update();
    // Actual UI stuff
    using namespace ImGui;
    // Infos about the ghost
    Text("Ghost name: %s", ghost.identity.getFullName().c_str());
    Text("Ghost age: %u", ghost.identity.age);
    Text("Ghost aggression: %f", ghost.getAggression());
    Text("Ghost type: %s", ghost.identity.behavior->name.c_str());
    if (Button("Regenerate")) {
        ghost.identity.generate(ghost.getLevelManager(), ghost.rng.getUInt());
    }
    Separator();
    int nsanity = levelManager.sanity;
    if (InputInt("Sanity", &nsanity)) {
        levelManager.sanity = std::clamp(nsanity, 0, 100);
    }
}
}
