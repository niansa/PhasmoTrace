#ifndef __WIN32
#include "PhasmoTrace/Tracer.hpp"
#include "QuickDebug/QuickDebug.hpp"
#include "PhasmoTrace/ProcPipe.hpp"

#include <string_view>
#include <sstream>
#include <fstream>
#include <filesystem>
#include <stdexcept>



namespace PhasmoTrace {
QuickDebug::Addr Tracer::findRelativeFunctionAddressByName(std::string_view name) const {
    const Function *fnc = nullptr;
    for (const auto& it : knownFunctions) {
        if (it.name == name) {
            fnc = &it;
        }
    }
    if (!fnc) {
        throw std::runtime_error("Failed to find function: "+std::string(name));
    }
    return fnc->addr;
}

void Tracer::patch_gameassembly(std::string_view gameassembly_path) {
    logger.log(Loglevel::info, "Preparing to patch GameAssembly...");
    std::filesystem::path gameAsmP = gameassembly_path;
    // Care about backup file stuff...
    std::filesystem::path gameAsmOriginalP;
    {
        logger.log(Loglevel::info, "Ensuring backup...");
        auto froot = gameAsmP.parent_path();
        std::string fname = gameAsmP.filename().string();
        gameAsmOriginalP = froot.parent_path().parent_path()/("Phasmophobia-"+fname+"-original");
        if (std::filesystem::exists(gameAsmOriginalP)) {
            // Replace non-original file
            std::filesystem::copy_file(gameAsmOriginalP, gameAsmP, std::filesystem::copy_options::overwrite_existing);
        } else {
            // Create backup of original file
            std::filesystem::copy_file(gameAsmP, gameAsmOriginalP, std::filesystem::copy_options::overwrite_existing);
        }
    }
    // Open file in rizin
    logger.log(Loglevel::info, "Launching rizin...");
    ProcPipe<true, false, false> rizin("rizin", "rizin", gameAsmP.string().c_str());
    rizin.send("oo+\n");
    // Find address to patch
    logger.log(Loglevel::info, "Patching...");
    // Send patches to rizin
    std::ostringstream patch;
    patch << "s 0x180000000 + " << findRelativeFunctionAddressByName(game.getInitialFunction()) << "\n"
             "wx " << std::hex << unsigned(QuickDebug::SimpleInstructions::breakpoint) << std::dec << '\n';
    for (const auto funcName : game.getExtraEarlyRetVoid()) {
        patch << "s 0x180000000 + " << findRelativeFunctionAddressByName(funcName) << "\n"
                 "wx " << std::hex << unsigned(QuickDebug::SimpleInstructions::ret) << std::dec << '\n';
    }
    for (const auto funcName : game.getExtraEarlyRetFalse()) {
        patch << "s 0x180000000 + " << findRelativeFunctionAddressByName(funcName) << "\n"
                 "wx 48c7c000000000c3\n";
    }
    for (const auto funcName : game.getExtraEarlyRetTrue()) {
        patch << "s 0x180000000 + " << findRelativeFunctionAddressByName(funcName) << "\n"
                 "wx 48c7c001000000c3\n";
    }
    rizin.send(std::move(patch).str());
    // Exit rizin safely
    logger.log(Loglevel::info, "Patch sent, waiting for rizin to complete...");
    rizin.send("q\n");
    rizin.waitExit();
    logger.log(Loglevel::info, "Patch has been applied and rizin has exited!");
    // Add magic
    logger.log(Loglevel::info, "Adding magic...");
    std::fstream gameAsm(gameAsmP, std::ios::openmode::_S_out | std::ios::openmode::_S_bin | std::ios::openmode::_S_app);
    // Write last 4 bytes
    const char magic[4] = {'P', 'H', 'S', 'T'};
    gameAsm.write(magic, sizeof(magic));
}
}
#endif
