#include "PhasmoEngine/GhostIdentity.hpp"
#include "PhasmoEngine/GhostBehavior.hpp"
#include "PhasmoTrace/Random.hpp"

#include <memory>



namespace PhasmoEngine {
const static std::vector<std::string_view> firstNamesMale = {"Jack", "Brian", "Richard", "Luke", "Russell", "Larry", "Carlos", "Robert", "Jason", "Daniel", "Donald", "Steven", "David", "Christopher", "Anthony", "James", "Mark", "Justin", "Corey", "Joseph", "Kenneth", "Charles", "Gary", "George", "John", "Benjamin", "Walter", "Kevin", "Jerry", "Billy", "Eric", "Harold", "Ronald", "William", "Pater", "Paul", "Michael", "Edward", "Thomas", "Raymond"},
                                           firstNamesFemale = {"Ruth", "Betty", "Lisa", "Georgia", "Dorothy", "Jennifer", "Karen", "Barbara", "Julie", "Ann", "Nancy", "Lucy", "Judy", "Gloria", "Nellie", "Megan", "Susan", "Mary", "Becky", "Patricia", "Jane", "Carla", "Sandra", "Marcia", "Ellen", "Linda", "Margaret", "Maria", "Helen", "Donna", "Elizabeth", "Holly", "Stacey", "Ella", "Carol"},
                                           lastNames = {"Hall", "Anderson", "Wright", "Young", "Carter", "Brown", "Taylor", "Lewis", "Smith", "Moore", "Williams", "Johnson", "Dixon", "Jones", "Jackson", "Garcia", "Roberts", "Walker", "Harris", "Martin", "Clarke", "Baker", "Miller", "Hill", "White", "Robinson", "Wilson", "Martinez", "Thompson"};


void GhostIdentity::generate(LevelManager *levelManager, unsigned seed) {
    PhasmoTrace::RandomGenerator rng;
    rng.seed(seed);

    // Get random gender
    gender = static_cast<Gender>(rng.getBool(0.5f));

    // Get name list to use
    const auto& firstNames = (gender == male)?firstNamesMale:firstNamesFemale;

    // Get random first and last name
    firstName = firstNames[rng.getUInt(firstNames.size())];
    lastName = lastNames[rng.getUInt(lastNames.size())];

    // Get random age
    age = rng.getUInt(10, 90);

    // Get random type
    type = static_cast<GhostType>(rng.getUInt(unsigned(GhostType::_lowest), unsigned(GhostType::_highest)));

    // Create that type
    behavior = getGhostBehavior(type);

    // Get random agression
    agression = rng.getDouble(0.8f, 1.3f);

    // Get ghost sounds
    huntSound = levelManager->getHuntSound(rng.getUInt(levelManager->getAvailableHuntSounds() - 1));
}
}
