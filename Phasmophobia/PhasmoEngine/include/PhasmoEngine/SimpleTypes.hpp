#ifndef _PHASMOENGINE_SIMPLETYPES_HPP
#define _PHASMOENGINE_SIMPLETYPES_HPP
#include <cstdint>
#include <cstddef>
#include <vector>



namespace PhasmoEngine {
namespace Evidence {
using Type = uint8_t;
enum : Type {
    EMFLevelFive =   0b00000001,
    DOTSProjection = 0b00000010,
    Fingerprints =   0b00000100,
    GhostOrbs =      0b00001000,
    GhostWriting =   0b00010000,
    SpiritBox =      0b00100000,
    FreezingTemps =  0b01000000
};

inline bool hasEvidence(Type list, Type evidence) {
    return list & evidence;
}
}


namespace InteractionType {
using Type = uint8_t;
enum : Type {
    touch = 0b10,
    throw_ = 0b01,
    any = 0b11,
    none = 0b00
};
}


enum class RevealMode {
    standing,
    chasing,
    airball
};


enum class EMFLevel {
    basic = 1,
    touch = 2,
    throw_ = 3,
    reveal = 4,
    five = 5
};

constexpr unsigned emfTimeout = 20000;


enum class GhostType {
    Spirit,
    Wraith,
    Phantom,
    Poltergeist,
    Banshee,
    Jinn,
    Mare,
    Revenant,
    Shade,
    Demon,
    Yurei,
    Oni,
    Yokai,
    Hantu,
    Goryo,
    Myling,
    Onryo,
    Twins,
    Raiju,
    Obake,
    Mimic,
    _lowest = Spirit,
    _highest = Mimic
};


enum class GhostState {
    local,
    roam,
    reveal,
    hunt
};


struct Player {
    virtual void kill() = 0;
    virtual float getDistanceToGhost() = 0;
    virtual unsigned getSanity() {return 100;}
    virtual void setSanity(unsigned sanity) {}
};

struct PlayerWDistance {
    Player *player = nullptr;
    float distance;

    bool hasValue() {
        return player;
    }
};


struct Equipment {
    virtual bool isTurnedOn() {return false;}
    virtual Player *getHoldingPlayer() {return nullptr;}
};

struct EquipmentWDistance {
    Equipment *equipment;
    float distance;
};


struct Prop {
    virtual void setLinearVelocity(float x, float y, float z) {}
};

struct PropWDistance {
    Prop *prop;
    float distance;
};


struct Door {
    virtual bool isOpened() {return true;}
    virtual void touch(float direction) {}
    virtual void smash(float direction) {}
};


struct Light {
    virtual bool isTurnedOn() {return false;}
    virtual void setTurnedOn(bool) {}
};


struct Room {
    virtual bool isPlayerInside(Player*) {return false;}
    virtual bool isGhostInside() {return false;}
    virtual std::vector<Light*> getLights() {return {};}
    virtual std::vector<Door*> getDoors() {return {};}
    virtual bool hasLightsTurnedOn() {return false;}
    virtual void turnOnAllLights() {}
    virtual void turnOffAllLights() {}
};

struct House {
    virtual bool isPlayerInside(Player*) {return true;}
    virtual bool isEquipmentInside(Equipment*) {return true;}
    virtual void turnOffAllLights() {}
    virtual std::vector<Room*> getRooms() {return {};}
};


struct Sound {
    virtual void play() {}
    virtual bool isPlaying() {return false;}
    virtual void stop() {}
};


struct LevelManager {
    virtual class Ghost *getGhost() {return nullptr;}
    virtual House *getHouse() {return nullptr;}
    virtual std::vector<Player*> getPlayers() {return {};}
    virtual size_t getAvailableHuntSounds() {return 0;}
    virtual size_t getAvailableRevealSounds() {return 0;}
    virtual size_t getAvailableHissSounds() {return 0;}
    virtual Sound* getHuntSound(unsigned index) {return nullptr;}
    virtual Sound* getRevealSound(unsigned index) {return nullptr;}
    virtual Sound* getHissSound(unsigned index) {return nullptr;}

    virtual unsigned getTeamSanity() {
        auto players = getPlayers();
        if (!players.empty()) {
            unsigned fres = 0;
            for (auto player : players) {
                fres += player->getSanity();
            }
            return fres / players.size();
        } else {
            return 100;
        }
    }
};


class GhostStateScript {
    Ghost *ghost;

public:
    GhostStateScript(Ghost *ghost) : ghost(ghost) {}

    virtual void Initialize() = 0;
    virtual void Deinitialize() {};
};
}
#endif
