#include "Tracer.hpp"
#include "il2cpp.h"
#include "magic_enum.hpp"
#include "QuickDebug/QuickDebug.hpp"

#include <string>
#include <string_view>
#include <vector>
#include <functional>
#include <mutex>
#include <thread>
#include <Urho3D/Urho3DAll.h>



extern "C" {
// Basic declarations
struct Value {
    int lastKnown;
    std::optional<int> override;
};

// RBFX Application
struct rbfxApp : public Application {
    URHO3D_OBJECT(rbfxApp, Application);

    Scene *scene = nullptr;
    Node *cameraNode, *hand;
    Model *box;
    Timer resetTimer;
    bool showLocal = true;
    bool showRemote = true;

    // Initialization
    explicit rbfxApp(Context* context) : Application(context) {
        engineParameters_[EP_RESOURCE_PREFIX_PATHS] = "bin";
        engineParameters_[EP_VSYNC] = true;
        engineParameters_[EP_FULL_SCREEN] = false;
    }

    void Start() final {
        // Handlers
        SubscribeToEvent(E_UPDATE, URHO3D_HANDLER(rbfxApp, HandleUpdate));

        // Reset
        reset();

        // Load box model
        box = GetSubsystem<ResourceCache>()->GetResource<Model>("Models/Box.mdl");
    }

    // Runtime
    void HandleUpdate(StringHash, VariantMap&) {
        // update queue
        {
            std::scoped_lock L(onNextUpdateMutex);
            for (const auto& fnc : onNextUpdate) {
                fnc();
            }
            onNextUpdate.clear();
        }
        // Periodic reset
        if (resetTimer.GetMSec(false) > 2500) {
            auto t = cameraNode->GetWorldTransform();
            reset();
            cameraNode->SetWorldTransform(t);
        }
        // input
        auto input = GetSubsystem<Input>();
        if (input->GetKeyPress(Key::KEY_L)) {
            showLocal = !showLocal;
        }
        if (input->GetKeyPress(Key::KEY_R)) {
            showRemote = !showRemote;
        }
        // Object grabbing
        if (input->GetMouseButtonClick(MouseButtonFlags::Enum::MOUSEB_LEFT)) {
            if (!hand) {
                // Grab item
                auto *ui = GetSubsystem<UI>();
                auto *graphics = GetSubsystem<Graphics>();
                IntVector2 pos = ui->GetUICursorPosition();
                // Raycast
                auto ray = cameraNode->GetComponent<Camera>()->GetScreenRay(float(pos.x_) / graphics->GetWidth(), float(pos.y_) / graphics->GetHeight());
                ea::vector<RayQueryResult> results;
                RayOctreeQuery query(results, ray, RAY_TRIANGLE, 20.0f, DRAWABLE_GEOMETRY);
                scene->GetComponent<Octree>()->RaycastSingle(query);
                // Get first result
                if (!results.empty()) {
                    auto node = results[0].node_;
                    hand = node;
                    auto t = node->GetWorldTransform();
                    cameraNode->AddChild(node);
                    node->SetWorldTransform(t);
                }
            } else {
                auto t = hand->GetWorldTransform();
                scene->AddChild(hand);
                hand->SetWorldTransform(t);
                hand = nullptr;
            }
        }
    }

    // Helpers
    void reset() {
        // Clear old scene
        if (scene) {
            hand = nullptr;
            scene->Clear();
            delete scene;
        }

        // Make new scene
        scene = new Scene(context_);
        scene->CreateComponent<Octree>();

        // Make camera
        cameraNode = scene->CreateChild("Camera");
        auto camera = cameraNode->CreateComponent<Camera>();
        cameraNode->CreateComponent<FreeFlyController>();

        // Make sun
        auto sun = scene->CreateChild("Sun");
        auto sunLight = sun->CreateComponent<Light>();
        sunLight->SetLightType(LightType::LIGHT_DIRECTIONAL);
        sun->SetRotation(Quaternion(Vector3(45.0f, 45.0f, 45.0f)));

        // Configure viewport
        auto* renderer = GetSubsystem<Renderer>();
        SharedPtr<Viewport> viewport(new Viewport(context_, scene, camera));
        renderer->SetViewport(0, viewport);

        // Reset timer
        resetTimer.Reset();
    }

    void createBox(Node *node) {
        auto staticModel = node->CreateComponent<StaticModel>();
        staticModel->SetModel(box);
        staticModel->SetMaterial(new Material(context_));
    }

    void colorBox(Node *node, const Color& color) {
        auto material = node->GetComponent<StaticModel>()->GetMaterial();
        material->SetShaderParameter("MatDiffColor", Color(color));
    }

    Node *getBoxByName(const ea::string& name) {
        auto fres = scene->GetChild(name);
        if (!fres && hand && hand->GetName() == name) {
            return hand;
        }
        return fres;
    }

    std::vector<std::function<void()>> onNextUpdate;
    std::mutex onNextUpdateMutex;
};

// Basic definitions
std::string_view PT_pluginName = "Phasmophobia scene stuff",
                 PT_pluginVersion = "V0.1-WIP",
                 PT_pluginDescription = "Idk";

// Globals
PhasmoTrace::Tracer *tracer;

// Settings
std::unordered_map<std::string, Value> pairs;

// Containers
static SharedPtr<rbfxApp> app;

// Helpers
void activateBreakpointWithName(std::string_view str) noexcept {
    tracer->addToActionQueue([str] () {
        tracer->getTarget().activateBreakpoint(tracer->getTarget().getBreakpointsByName(str)[0]);
    });
}

// Callbacks
void init() noexcept {
    activateBreakpointWithName("Photon.Pun.PhotonTransformView$$Update");
    // Run rbfx applicatio
    std::thread([] () {
        SharedPtr<Context> context(new Context());
        app = new rbfxApp(context);
        app->Run();
    }).detach();
}

void onBreakpoint(QuickDebug::BreakpointList& brkl) {
    for (auto& brk : brkl.getVector()) {
        if (brk->getName() == "Photon.Pun.PhotonTransformView$$Update") {
            const auto ThisPtr = tracer->getTarget().getRegisters().rcx;
            auto This = tracer->getTarget().readObject<Il2Cpp::Object<Il2Cpp::Photon::Pun::PhotonTransformView>>(ThisPtr);
            std::scoped_lock L(app->onNextUpdateMutex);
            // Networked
            {
                const auto& position = This.fields.m_NetworkPosition;
                const auto& rotation = This.fields.m_NetworkRotation;
                app->onNextUpdate.push_back([GameThisPtr = ThisPtr, GameThis = This, position, rotation] () mutable {
                    if (app->showRemote) {
                        auto node = app->getBoxByName(ea::to_string(GameThisPtr)+"_remote");
                        if (!node) {
                            node = app->scene->CreateChild(ea::to_string(GameThisPtr)+"_remote");
                            app->createBox(node);
                            app->colorBox(node, Color::WHITE);
                        }
                        node->SetWorldPosition({position.x, position.y, position.z});
                        node->SetWorldRotation({rotation.x, rotation.y, rotation.z, rotation.w});
                    }
                });
            }
            // Local
            {
                const auto& position = This.fields.m_StoredPosition;
                app->onNextUpdate.push_back([GameThisPtr = ThisPtr, GameThis = This, position] () mutable {
                    if (app->showLocal) {
                        auto node = app->getBoxByName(ea::to_string(GameThisPtr)+"_local");
                        if (!node) {
                            node = app->scene->CreateChild(ea::to_string(GameThisPtr)+"_local");
                            app->createBox(node);
                            app->colorBox(node, Color::BLUE);
                            node->SetScale({0.8f, 0.8f, 0.8f});
                        }
                        node->SetWorldPosition({position.x, position.y, position.z});
                    }
                });
            }
        }
    }
}
}

