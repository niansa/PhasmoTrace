#ifndef PLUGIN_HPP
#define PLUGIN_HPP
#include "dlhandle.hpp"

#include <string>
#include <string_view>
#include <vector>
#include <memory>
#include <filesystem>



namespace PhasmoTrace {
class Tracer;

struct Plugin : public Dlhandle {
    Plugin(const std::string& fpath, Tracer& tracer);

    std::string_view getName() {
        auto str = get<std::string_view>("PT_pluginName");
        if (str) {
            return *str;
        } else {
            return "";
        }
    }
    std::string_view getVersion() {
        auto str = get<std::string_view>("PT_pluginVersion");
        if (str) {
            return *str;
        } else {
            return "";
        }
    }
    std::string_view getDescription() {
        auto str = get<std::string_view>("PT_pluginDescription");
        if (str) {
            return *str;
        } else {
            return "";
        }
    }
};

struct Game : public Dlhandle {
    Game(const std::string& fpath);

    std::string_view getName() {
        auto str = get<std::string_view>("PT_gameName");
        if (str) {
            return *str;
        } else {
            return "";
        }
    }
    std::string_view getInitialFunction() {
        auto str = get<std::string_view>("PT_gameInitialFunction");
        if (str) {
            return *str;
        } else {
            return "";
        }
    }
    std::string_view getInitialFunctionTriggerInstructions() {
        auto str = get<std::string_view>("PT_gameInitialFunctionTriggerInstructions");
        if (str) {
            return *str;
        } else {
            return "";
        }
    }
    const std::vector<std::string_view>& getExtraEarlyRetVoid() {
        auto strvec = get<std::vector<std::string_view>>("PT_gameExtraEarlyRetVoid");
        if (strvec) {
            return *strvec;
        } else {
            static const std::vector<std::string_view> empty_strvec = {};
            return empty_strvec;
        }
    }
    const std::vector<std::string_view>& getExtraEarlyRetFalse() {
        auto strvec = get<std::vector<std::string_view>>("PT_gameExtraEarlyRetFalse");
        if (strvec) {
            return *strvec;
        } else {
            static const std::vector<std::string_view> empty_strvec = {};
            return empty_strvec;
        }
    }
    const std::vector<std::string_view>& getExtraEarlyRetTrue() {
        auto strvec = get<std::vector<std::string_view>>("PT_gameExtraEarlyRetTrue");
        if (strvec) {
            return *strvec;
        } else {
            static const std::vector<std::string_view> empty_strvec = {};
            return empty_strvec;
        }
    }
};

struct PluginDesc {
    std::filesystem::path fpath;
    std::unique_ptr<Plugin> plugin = nullptr;

    PluginDesc(const std::string& fpath) : fpath(fpath) {}
    PluginDesc(const PluginDesc& o) : fpath(o.fpath) {}

    void load(Tracer &tracer) {
        plugin = std::make_unique<Plugin>(fpath.string(), tracer);
    }
    void unload() {
        plugin = nullptr;
    }
    void reload() {
        Tracer& tracer = **plugin->get<Tracer*>("tracer");
        unload();
        load(tracer);
    }
    bool isLoaded() const {
        return plugin.get();
    }
    Plugin *operator ->() {
        return plugin.get();
    }
    const Plugin *operator ->() const {
        return plugin.get();
    }
};
}
#endif // PLUGIN_HPP
