#ifndef _PHASMOPHOBIA_IL2CPP_H
#define _PHASMOPHOBIA_IL2CPP_H
#include "QuickDebug.hpp"
#include "PhasmoTrace/il2cpp.h"



namespace Il2Cpp {
#pragma pack(push)
#pragma pack(8)
namespace ES3 {
struct Settings {
    int location;
    QuickDebug::Addr path;
    int encryptionType;
    int compressionType;
    QuickDebug::Addr encryptionPassword;
    int directory;
    int format;
    bool prettyPrint;
    int bufferSize;
    QuickDebug::Addr encoding;
    bool saveChildren;
    bool typeChecking;
    bool safeReflection;
    int memberReferenceMode;
    int referenceMode;
    int serializationDepthLimit;
    QuickDebug::Addr assemblyNames;
};
}

namespace Photon {
namespace Pun {
struct PhotonStream {
    QuickDebug::Addr writeData;
    QuickDebug::Addr readData;
    int currentItem;
    bool isWriting;
};
}
namespace Realtime {
struct AppSettings {
    QuickDebug::Addr appIdRealtime;
    QuickDebug::Addr appIdChat;
    QuickDebug::Addr appIdVoice;
    QuickDebug::Addr appVersion;
    bool useNameServer;
    QuickDebug::Addr fixedRegion;
    QuickDebug::Addr bestRegionSummaryFromStorage;
    QuickDebug::Addr server;
    int port;
    QuickDebug::Addr proxyServer;
    unsigned char protocol;
    bool enableProtocolFallback;
    int authMode;
    bool enableLobbyStatistics;
    unsigned char networkLogging;
};
struct RoomOptions {
    bool isVisible;
    bool isOpen;
    unsigned char maxPlayers;
    int playerTtl;
    int emptyRoomTtl;
    bool cleanupCacheOnLeave;
    QuickDebug::Addr customRoomProperties;
    QuickDebug::Addr customRoomPropertiesForLobby;
    QuickDebug::Addr plugins;
    bool suppressRoomEvents;
    bool suppressPlayerInfo;
    bool publishUserId;
    bool deleteNullProperties;
    bool broadcastPropsChangeToAll;
};
struct Player {
    QuickDebug::Addr room;
    int actorNumber;
    bool isLocal;
    bool hasRejoined;
    QuickDebug::Addr nickName;
    QuickDebug::Addr userID;
    bool isInactive;
    QuickDebug::Addr customProperties;
    QuickDebug::Addr tagObject;
};
struct RoomInfo {
    bool RemovedFromList;
    QuickDebug::Addr customProperties;
    unsigned char maxPlayers;
    int emptyRoomTtl;
    int playerTtl;
    QuickDebug::Addr expectedUsers;
    bool isOpen;
    bool isVisible;
    bool autoCleanUp;
    QuickDebug::Addr name;
    int32_t masterClientId;
    QuickDebug::Addr propertiesListedInLobby;
    int32_t playerCount;
};
namespace GamePropertyKey {
constexpr uint8_t MaxPlayers = 255,
                  IsVisible = 254,
                  IsOpen = 253,
                  PlayerCount = 252,
                  Removed = 251,
                  PropsListedInLobby = 250,
                  CleanupCacheOnLeave = 249,
                  MasterClientId = 248,
                  ExpectedUsers = 247,
                  PlayerTtl = 246,
                  EmptyRoomTtl = 245;
}
}
}
#pragma pack(pop)

namespace Game {
struct ServerManager {
    QuickDebug::Addr characterIcons;
    QuickDebug::Addr loadingScreen;
    QuickDebug::Addr startGameButton;
    QuickDebug::Addr readyButton;
    QuickDebug::Addr readyText;
    QuickDebug::Addr selectJobText;
    QuickDebug::Addr startGameText;
    UnityEngine::Color enabledColour;
    UnityEngine::Color disabledColour;
    UnityEngine::Color32 ___________;
    UnityEngine::Color32 _10____________;
    UnityEngine::Color32 _11____________;
    QuickDebug::Addr photonView;
    QuickDebug::Addr _13____________;
    QuickDebug::Addr levelSelectionManager;
    QuickDebug::Addr mainManager;
    QuickDebug::Addr loadingAsyncManager;
    QuickDebug::Addr storeSDKManager;
    QuickDebug::Addr jobFinderButton;
    QuickDebug::Addr contractSelectionObject;
    QuickDebug::Addr difficultyText;
    QuickDebug::Addr levelSelectionText;
    QuickDebug::Addr _22____________;
    QuickDebug::Addr _23____________;
    int _24____________;
    QuickDebug::Addr serverMask;
    QuickDebug::Addr mainMask;
    QuickDebug::Addr storeObject;
    QuickDebug::Addr mainSelector;
    QuickDebug::Addr contractSelector;
    QuickDebug::Addr kickPlayerButtons;
    QuickDebug::Addr inviteCodeText;
    QuickDebug::Addr inviteText;
    QuickDebug::Addr hideButton;
    QuickDebug::Addr copyButton;
    QuickDebug::Addr _35____________;
};
struct LiftButton {
    QuickDebug::Addr anim;
    QuickDebug::Addr photonView;
    QuickDebug::Addr photonInteract;
    QuickDebug::Addr soundSource;
    QuickDebug::Addr exitLevel;
    QuickDebug::Addr truckRamp;
    float ___________;
    bool _7____________;
    bool _8____________;
    QuickDebug::Addr wallCollider;
    bool _10____________;
};
struct CursedItemsController : UnityEngine::Object {
    QuickDebug::Addr ouijaBoard;
    QuickDebug::Addr musicBox;
    QuickDebug::Addr tarotCards;
    QuickDebug::Addr summoningCircle;
    QuickDebug::Addr hauntedMirror;
    QuickDebug::Addr voodooDoll;
    QuickDebug::Addr ouijaBoardSpawnSpots;
    QuickDebug::Addr musicBoxSpawnSpots;
    QuickDebug::Addr tarotCardsSpawnSpots;
    QuickDebug::Addr summoningCircleSpawnSpots;
    QuickDebug::Addr hauntedMirrorSpawnSpots;
    QuickDebug::Addr voodooDollSpawnSpots;
};
}

namespace Photon {
namespace Pun {
struct PhotonMessageInfo {
    int timeInt;
    QuickDebug::Addr sender;
    QuickDebug::Addr photonView;
};
struct MonoBehaviourPun : UnityEngine::Object {
    QuickDebug::Addr pvCache;
};
struct PhotonTransformView : public MonoBehaviourPun {
    float distance;
    float angle;
    struct UnityEngine::Vector3 direction;
    struct UnityEngine::Vector3 networkPosition;
    struct UnityEngine::Vector3 storedPosition;
    struct UnityEngine::Quaternion networkRotation;
    bool synchronizePosition;
    bool synchronizeRotation;
    bool synchronizeScale;
    bool useLocal;
    bool firstTake;
};
}
}
}
#endif // _PHASMOPHOBIA_IL2CPP_H
