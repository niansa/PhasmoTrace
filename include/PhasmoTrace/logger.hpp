#ifndef _LOGGER_HPP
#define _LOGGER_HPP
#include "magic_enum.hpp"

#include <iostream>
#include <iomanip>
#include <string_view>



namespace QLog {
enum class Loglevel {
    none,
    error,
    warn,
    info,
    verbose,
    debug,
    trace,
    _default =
#   ifdef NDEBUG
        info
#   else
        debug
#   endif
};


/*
 * This class provides basic logging functionality
 */
template<class InstanceT>
struct Logger {
    Loglevel level = Loglevel::_default;
    InstanceT *inst_ptr;

    void log(Loglevel level, std::string_view message) {
        if (this->level >= level) {
            std::time_t t = std::time(nullptr);
            std::tm tm = *std::localtime(&t);
            std::cout << "(" << std::put_time(&tm, "%Y-%m-%d %H:%M:%S") << ") [" << typeid(InstanceT).name() << '@' << inst_ptr << "] " <<
#                        if MAGIC_ENUM_SUPPORTED
                         magic_enum::enum_name(
#                        endif
                             level
#                        if MAGIC_ENUM_SUPPORTED
                         )
#                        endif
                      << ": " << message << std::endl;
        }
    }
};

}
#endif
