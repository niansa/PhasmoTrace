#include "PhasmoTrace/Game/Phasmophobia.hpp"



extern "C" {
std::string_view PT_gameName = "Phasmophobia",
                 PT_gameInitialFunction = "PlayerStamina$$PlayOutOfBreathSound",
                 PT_gameInitialFunctionTriggerInstructions = "Sprint until you run out of stamina to activate them!";
//std::vector<std::string_view> PT_gameExtraEarlyRetFalse = {"System.IO.File$$Exists", "System.IO.Directory$$Exists"}; // Prevents MelonLoader detection
}

namespace Phasmophobia {
PhasmoTrace::Tracer *tracer;


void initGameBase(PhasmoTrace::Tracer *_tracer) {
    tracer = _tracer;
}


Item::Item(std::string_view name, std::string_view friendlyName, decltype(instances)::size_type instanceCount, std::variant<int, const char *> knownTriggerNoOrName, std::string_view startFunctionName)
   : name(name), friendlyName(friendlyName), startFunctionName(startFunctionName) {
    if (instanceCount == dynamicInstanceCount) {
        instanceRing = false;
    } else {
        instances = decltype(instances)(instanceCount);
        instanceRing = true;
    }
    clearInstances();
    instanceIterator = instances.begin();
    switch (knownTriggerNoOrName.index()) {
    case 0: triggerNo = std::get<int>(knownTriggerNoOrName); break;
    case 1: triggerNo = 0; knownTriggerFunctionName = std::get<const char *>(knownTriggerNoOrName); break;
    }
}
Item::Item(const Item& o) {
    name = o.name;
    friendlyName = o.friendlyName;
    trigger = o.trigger;
    triggerNo = o.triggerNo;
    knownTriggerFunctionName = o.knownTriggerFunctionName;
    disturbed = o.disturbed;
    size_t i = o.instanceIterator - o.instances.begin();
    instances = o.instances;
    instanceIterator = instances.begin() + i;
    instanceRing = o.instanceRing;
    startFunctionName = o.startFunctionName;
}
Item::Item(Item&& o) {
    name = std::move(o.name);
    friendlyName = std::move(o.friendlyName);
    trigger = o.trigger;
    triggerNo = o.triggerNo;
    knownTriggerFunctionName = std::move(o.knownTriggerFunctionName);
    disturbed = o.disturbed;
    size_t i = o.instanceIterator - o.instances.begin();
    instances = std::move(o.instances);
    instanceIterator = instances.begin() + i;
    instanceRing = o.instanceRing;
    startFunctionName = std::move(o.startFunctionName);
}

QuickDebug::Addr& Item::nextInstance() {
    if (instanceRing) {
        auto& instance = *instanceIterator;
        if (++instanceIterator == instances.end()) {
            instanceIterator = instances.begin();
        }
        return instance;
    } else {
        return instances.emplace_back(0);
    }
}
void Item::clearInstances() {
    if (instanceRing) {
        for (auto& instance : instances) {
            instance = 0;
        }
    } else {
        instances.clear();
    }
}
bool Item::hasInstances() const {
    if (instanceRing) {
        return instances[0] != 0;
    } else {
        return !instances.empty();
    }
}

void Item::findTrigger() {
    if (!knownTriggerFunctionName.empty()) {
        // Get specific function
        auto brkl = tracer->getTarget().getBreakpointsByName(knownTriggerFunctionName);
        if (!brkl.empty()) {
            trigger = brkl[0]->getAddr();
        }
    } else {
        // Function name not known, analyze all functions
        tracer->addToActionQueue([this] () {
            unsigned currentTriggerNo = 0;
            for (const auto& [addr, brkl] : tracer->getTarget().getBreakpoints()) {
                for (const auto& brk : brkl.getVector()) {
                    if (brk->getName().find(name+"$$") == 0) {
                        // Test if function executes RPC and is the right trigger
                        QuickDebug::Addr testaddr = 0;
                        tracer->findFunctionPointers(
                                    addr, 1000,
                                    {
                                        {testaddr, "Photon.Pun.PhotonView$$RPC"}
                                    }
                        );
                        if (testaddr && currentTriggerNo++ == triggerNo/*Not the best place to count, won't be changing it now*/) {
                            // It does! Get function signature
                            if (brk->getData().type() == typeid(const PhasmoTrace::Function*)) {
                                const auto& signature = std::any_cast<const PhasmoTrace::Function*>(brk->getData())->signature;
                                // Check that function takes no arguments
                                if (signature.find("("+name+"_o* __this, const MethodInfo* method);") != std::string::npos) {
                                    // We've found the trigger!
                                    trigger = addr;
                                    return;
                                }
                            }
                        }
                    }
                }
            }
        });
    }
}
void Item::use(unsigned instance) const {
    if (!isUsable()) {
        return;
    }
    tracer->lateAllocateTempBufferSpace(tracer->storeStringAt(tracer->getTempBufferSpace(), "Hello world!"));
    tracer->call(trigger, {instances.at(instance), 0});
}
}
