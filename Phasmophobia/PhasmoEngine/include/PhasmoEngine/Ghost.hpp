#ifndef _PHASMOENGINE_HPP
#define _PHASMOENGINE_HPP
#include "PhasmoTrace/Timer.hpp"
#include "PhasmoTrace/Random.hpp"
#include "PhasmoEngine/SimpleTypes.hpp"
#include "PhasmoEngine/GhostBehavior.hpp"
#include "PhasmoEngine/GhostIdentity.hpp"

#include <vector>
#include <memory>
#include <optional>



namespace PhasmoEngine {
class Ghost {
    PhasmoTrace::Timer stateTimer, interactionTimer, lastHuntTimer, lowFreqStepTimer;
    GhostState nextState;
    std::optional<unsigned> nextStateIn;
    bool nextRoamFar = false;

public:
    PhasmoTrace::RandomGenerator rng;
    GhostIdentity identity;

    void autoPrepare() {
        rng.seed();
        identity.generate(getLevelManager(), rng.getUInt());
        setupNextState();
    }

    virtual LevelManager *getLevelManager() = 0;
    virtual bool canSeePlayer(PlayerWDistance) {return false;}
    virtual void updateClosestPlayer() {}
    virtual PlayerWDistance getClosestPlayer() {return {};}
    virtual void tryInteract(InteractionType::Type type = InteractionType::any) {}
    virtual std::vector<PropWDistance> getCloseProps() {return {};}
    virtual std::vector<EquipmentWDistance> getCloseEquipment() {return {};}
    virtual Room *getCurrentRoom() {return nullptr;}
    virtual void setState(GhostState state) {}
    virtual GhostState getState() {return GhostState::local;}
    virtual void setVisible() {}
    virtual bool isVisible() {return false;}

    void update();
    void throwProp(Prop *prop);
    void setupNextState();

    void markNextRoamAsFar() {
        nextRoamFar = true;
    }

    float getAggression() {
        return std::max(0.01f, (-(float(getLevelManager()->getTeamSanity())-100))/100) * identity.agression * identity.behavior->agression;
    }

    bool hasNextState() {
        return nextStateIn.has_value();
    }
    void clearNextState() {
        nextStateIn.reset();
    }
    void switchState() {
        clearNextState();
        setState(nextState);
        setupNextState();
    }
    void tryStateSwitch() {
        if (hasNextState() && stateTimer.get() > nextStateIn) {
            switchState();
        }
    }
    void setNextState(GhostState nState, unsigned in) {
        stateTimer.reset();
        nextState = nState;
        nextStateIn = in;
    }
    float getStateDuration() {
        return nextStateIn.value_or(0);
    }
};
}
#endif
