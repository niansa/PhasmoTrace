#include "PhasmoEngine/GhostBehavior.hpp"
#include "PhasmoEngine/Ghost.hpp"



namespace PhasmoEngine {
namespace GhostBehaviors {
float Default::getCurrentSpeed() {
    if (ghost->getState() == GhostState::hunt) {
        auto playerToChase = getPlayerToChase();
        if (playerToChase.hasValue() && ghost->canSeePlayer(playerToChase)) {
            speedup = std::min(speedup + 0.0015, 3.5);
        } else if (speedup != 0.0) {
            speedup = std::max(speedup - 0.005, 0.0);
        }
        return 1.0f + speedup;
    } else {
        return 1.0;
    }
}

PlayerWDistance Default::getPlayerToChase() {
    ghost->updateClosestPlayer();
    return ghost->getClosestPlayer();
}

EMFLevel Default::getEMFLevel(EMFLevel expected) {
    if (hasEvidence(Evidence::EMFLevelFive) && ghost->rng.getBool(0.25)) {
        return EMFLevel::five;
    } else {
        return expected;
    }
}

void Default::onStateChange(const std::string& nState) {
    if (nState == "Hunt") {
        speedup = 0.0;
    }
}

float Default::getBlinkSpeed() {
    return ghost->rng.getDouble(80.0, 300.0);
}

float Default::getThrowPower() {
    return ghost->rng.getDouble(0.25, 1.75);
}

RevealMode Default::getRevealMode(Player *player) {
    // DNV
    auto distance = player->getDistanceToGhost();
    if (distance < 2.0) {
        return RevealMode::standing;
    } else if (distance < 10.0) {
        return RevealMode::chasing;
    } else {
        return RevealMode::airball;
    }
}


void Wraith::FrequentUpdate(float) {

}


void Poltergeist::onInteraction(InteractionType::Type type) {
    // Do multi-throw sometimes
    //TODO
}


float Phantom::getBlinkSpeed() {
    return ghost->rng.getDouble(1000.0, 2000.0);
}


float Jinn::getCurrentSpeed() {
    if (ghost->getState() == GhostState::hunt) {
        // Add speed if the ghost is able to see the player
        auto player = getPlayerToChase();
        if (ghost->canSeePlayer(player) && player.distance > 2.0) {
            return 2.5;
        }
    }
    return Default::getCurrentSpeed();
}


void Mare::onStateChange(const std::string &nState) {
    Default::onStateChange(nState);
    // If there are no lights turned on in room, far roam
    if (ghost->getCurrentRoom()->hasLightsTurnedOn()) {
        ghost->markNextRoamAsFar();
    }
}

int Mare::getHuntMultiplier() {
    return ghost->getCurrentRoom()->hasLightsTurnedOn()?-10:10;
}


float Revenant::getCurrentSpeed() {
    if (ghost->getState() == GhostState::hunt) {
        // Add speed if the ghost is able to see the player
        if (ghost->canSeePlayer(getPlayerToChase())) {
            return 2.0;
        } else {
            return 0.5;
        }
    }
    return 1.0;
}


float Shade::getAgressionMultiplier() {
    ghost->updateClosestPlayer();
    if (ghost->getClosestPlayer().distance < 7.5f/*TBV*/) {
        return 0.5;
    } else {
        return 1.0;
    }
}


float Oni::getThrowPower() {
    return ghost->rng.getDouble(0.75, 2.0);
}

float Oni::getAgressionMultiplier() {
    ghost->updateClosestPlayer();
    if (ghost->getClosestPlayer().distance < 7.5f/*TBV*/) {
        return 1.5;
    } else {
        return 1.0;
    }
}


float Raiju::getCurrentSpeed() {
    auto bSpeed = Default::getCurrentSpeed();
    if (ghost->getState() == GhostState::hunt) {
        // Check if there is any turned on electronic turned on nearby
        for (auto equipment : ghost->getCloseEquipment()) {
            if (equipment.distance <= 3.0) {
                if (ghost->getLevelManager()->getHouse()->isEquipmentInside(equipment.equipment)) {
                    if (equipment.equipment->isTurnedOn()) {
                        // Make it faster
                        bSpeed += 1.5;
                        break;
                    }
                }
            }
        }
    }
    // Return final speed
    return bSpeed;
}

bool Obake::hasEvidence(Evidence::Type checkedFor) {
    auto fres = Default::hasEvidence(checkedFor);
    if (checkedFor == Evidence::Fingerprints && ghost->rng.getBool(0.25)) {
        fres = false;
    }
    return fres;
}
}

std::unique_ptr<GhostBehavior> getGhostBehavior(GhostType t) {
#   define handlecase(name) case GhostType::name: return std::make_unique<GhostBehaviors::name>()
    switch (t) {
    handlecase(Spirit);
    handlecase(Wraith);
    handlecase(Phantom);
    handlecase(Poltergeist);
    handlecase(Banshee);
    handlecase(Jinn);
    handlecase(Mare);
    handlecase(Revenant);
    handlecase(Shade);
    handlecase(Demon);
    handlecase(Yurei);
    handlecase(Oni);
    handlecase(Yokai);
    handlecase(Hantu);
    handlecase(Goryo);
    handlecase(Myling);
    handlecase(Onryo);
    handlecase(Twins);
    handlecase(Raiju);
    handlecase(Obake);
    handlecase(Mimic);
    default: return nullptr;
    }
}
}
