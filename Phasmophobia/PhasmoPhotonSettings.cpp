#include "PhasmoTrace/Tracer.hpp"
#include "PhasmoTrace/Game/Phasmophobia_il2cpp.h"
#include "QuickDebug.hpp"
#include "imgui.h"

#include <string_view>
#include <utility>
#include <memory>



// Basic definitions
extern "C" {
std::string_view PT_pluginName = "Phasmophobia Photon Settings",
                 PT_pluginVersion = "V1.0",
                 PT_pluginDescription = "A Phasmophobia Photon settings manager.";

// Globals
PhasmoTrace::Tracer *tracer;
}

// Settings
class AppSettings {
    QuickDebug::Addr strKlass;

    void il2CppStringToBuf(QuickDebug::Addr o, char *buf) {
        if (o) {
            auto cppStr = tracer->getStrAt(o);
            auto cStr = cppStr.c_str();
            memcpy(buf, cStr, cppStr.size());
            buf[cppStr.size()] = '\0';
            strKlass = tracer->getTarget().readObject<Il2Cpp::Object<Il2Cpp::BuiltIn::Empty>>(o).object.klass;
        } else {
            buf[0] = 0;
        }
    }
    void bufToIl2CppString(const char *buf, QuickDebug::Addr *o) const {
        if (buf[0]) {
            std::string cppStr(buf);
            if (*o) {
                tracer->storeStringAt(*o, cppStr, strKlass);
            } else {
                *o = tracer->getTempBufferSpace();
                tracer->lateAllocateTempBufferSpace(tracer->storeStringAt(*o, cppStr, strKlass));
            }
        } else {
            *o = 0;
        }
    }

public:
    char appIdRealtime[64];
    char appIdChat[64];
    char appIdVoice[64];
    char appVersion[64];
    bool useNameServer;
    char fixedRegion[64];
    char bestRegionSummaryFromStorage[64];
    char server[64];
    int port;
    char proxyServer[64];
    int protocol;
    bool enableProtocolFallback;
    int authMode;
    bool enableLobbyStatistics;
    int networkLogging;

#   define CONVLIST \
    CONVSTRFIELD(appIdRealtime); \
    CONVSTRFIELD(appIdChat); \
    CONVSTRFIELD(appIdVoice); \
    CONVSTRFIELD(appVersion); \
    CONVBOOLFIELD(useNameServer); \
    CONVSTRFIELD(fixedRegion); \
    CONVSTRFIELD(bestRegionSummaryFromStorage); \
    CONVSTRFIELD(server); \
    CONVINTFIELD(port); \
    CONVSTRFIELD(proxyServer); \
    CONVINTFIELD(protocol); \
    CONVBOOLFIELD(enableProtocolFallback); \
    CONVINTFIELD(authMode); \
    CONVBOOLFIELD(enableLobbyStatistics); \
    CONVINTFIELD(networkLogging);

    void fromIl2CppClass(const Il2Cpp::Photon::Realtime::AppSettings& o) {
#       define CONVBOOLFIELD(field) field = o.field
#       define CONVINTFIELD(field) field = o.field
#       define CONVSTRFIELD(field) il2CppStringToBuf(o.field, field)
        CONVLIST
#       undef CONVBOOLFIELD
#       undef CONVINTFIELD
#       undef CONVSTRFIELD
    }

    void toIl2CppClass(Il2Cpp::Photon::Realtime::AppSettings& o) const {
#       define CONVBOOLFIELD(field) o.field = field
#       define CONVINTFIELD(field) o.field = field
#       define CONVSTRFIELD(field) bufToIl2CppString(field, &o.field)
        CONVLIST
#       undef CONVBOOLFIELD
#       undef CONVINTFIELD
#       undef CONVSTRFIELD
    }

    void renderUI() {
        using namespace ImGui;
#       define CONVBOOLFIELD(field) Checkbox(#field, &field)
#       define CONVINTFIELD(field) InputInt(#field, &field)
#       define CONVSTRFIELD(field) InputText(#field, field, sizeof(field))
        CONVLIST
#       undef CONVBOOLFIELD
#       undef CONVINTFIELD
#       undef CONVSTRFIELD
    }

#   undef CONVLIST
} *appSettings;

// Callbacks
extern "C" {
void init() noexcept {
    appSettings = nullptr;
    tracer->activateAllBreakpointsWithName("Photon.Pun.PhotonNetwork$$ConnectUsingSettings");
}
void deinit() noexcept {
    if (appSettings) {
        delete appSettings;
    }
}

void onBreakpoint(QuickDebug::BreakpointList& brkl) {
    // Handle breakpoints
    auto brk = brkl.getVector()[0];
    std::string_view signature;
    if (brk->getData().type() == typeid(const PhasmoTrace::Function*)) {
        signature = std::any_cast<const PhasmoTrace::Function*>(brk->getData())->signature;
    }
    if (signature == "bool Photon_Pun_PhotonNetwork__ConnectUsingSettings (Photon_Realtime_AppSettings_o* appSettings, bool startInOfflineMode, const MethodInfo* method);") {
        // Get settings
        auto settingsBuf = tracer->getTarget().getRegisters().rcx;
        auto settings = tracer->getTarget().readObject<Il2Cpp::Object<Il2Cpp::Photon::Realtime::AppSettings>>(settingsBuf);
        if (appSettings) {
            appSettings->toIl2CppClass(settings.fields);
            tracer->getTarget().writeObject(settingsBuf, settings);
        } else {
            appSettings = new AppSettings;
            appSettings->fromIl2CppClass(settings.fields);
        }
    }
}

void onUIRender() noexcept {
    using namespace ImGui;
    // Render UI
    if (!appSettings) {
        TextUnformatted("Click \"Multiplayer\" in main menu to use this plugin.");
    } else {
        appSettings->renderUI();
        if (Button("Reset")) {
            if (appSettings) {
                delete std::exchange(appSettings, nullptr);
            }
        }
        Separator();
        TextUnformatted("Click \"Multiplayer\" in main menu to apply these settings.");
    }
}
}
