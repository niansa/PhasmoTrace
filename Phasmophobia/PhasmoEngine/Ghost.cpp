#include "PhasmoEngine/Ghost.hpp"

#include <algorithm>



namespace PhasmoEngine {
void Ghost::update() {
    auto lfst = lowFreqStepTimer.get();
    if (lfst > 2500) {
        // Interact
        if ((isVisible() || rng.getBool(0.01f * getAggression())) && interactionTimer.get() > identity.behavior->interactionCooldown) {
            tryInteract();
        }
        // Run behavior update
        identity.behavior->FrequentUpdate(lfst);
        // Reset step timer
        lowFreqStepTimer.reset();
    }
    // Potential state switch
    tryStateSwitch();
}

void Ghost::setupNextState() {
    switch (getState()) {
    case GhostState::local: {
        GhostState nState;
        auto teamSanity = getLevelManager()->getTeamSanity();
        if (teamSanity < identity.behavior->sanityThreshold + identity.behavior->getHuntMultiplier() && lastHuntTimer.get() > (identity.behavior->huntCooldown - 1000) && rng.getBool(teamSanity>25?0.25f:0.5f)) {
            nState = GhostState::hunt;
        } else if (rng.getBool(0.025f*getAggression())) {
            nState = GhostState::reveal;
        } else  {
            nState = GhostState::roam;
        }
        setNextState(nState, rng.getUInt(5000, std::clamp<unsigned>(20000.0f/(getAggression()*4.0f), 5000, 30000)));
    } break;
    case GhostState::roam: {
        setNextState(GhostState::local, rng.getUInt(5000, std::max<unsigned>(20000.0f*getAggression(), 5000)));
    } break;
    case GhostState::reveal: {
        setNextState(GhostState::local, rng.getUInt(2500, std::max<unsigned>(15000.0f*getAggression(), 1500)));
    } break;
    case GhostState::hunt: {
        setNextState(GhostState::local, identity.behavior->huntDuration);
    } break;
    }
}

void Ghost::throwProp(Prop *prop) {
    auto power = identity.behavior->getThrowPower();
    float x = rng.getDouble(-1.5, 1.5) * power;
    float y = rng.getDouble(1.5) * power;
    float z = rng.getDouble(-1.5, 1.5) * power;
    prop->setLinearVelocity(x, y, z);
}
}
