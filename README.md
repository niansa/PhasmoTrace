# PhasmoTrace

## Usage

    # Compilation
    mkdir build
    cd build
    cmake ..
    make -j$(nproc)
    # Executable preparation
    ./phasmotrace prepare <GameAssembly.dll> <il2cppdumper generated script.json>
    # Running
    ./phasmotrace run <process ID> <il2cppdumper generated script.json>

Now, run out of stamina once so to let phasmotrace find the games base address. An automatic way is planned.

## By the way...

Read this thread on uc if you want to learn more:
https://www.unknowncheats.me/forum/phasmophobia/
