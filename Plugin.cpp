#include "PhasmoTrace/Namespace.hpp"
#include "PhasmoTrace/Plugin.hpp"
#include "PhasmoTrace/Tracer.hpp"



namespace PhasmoTrace {
Plugin::Plugin(const std::string& fpath, Tracer& tracer) : Dlhandle(fpath) {
    *get<Tracer*>("tracer") = &tracer;
    auto init = get<void*()>("init");
    if (init) {
        init();
    }
}

Game::Game(const std::string& fpath) : Dlhandle(fpath) {
    auto init = get<void*()>("init");
    if (init) {
        init();
    }
}
}
