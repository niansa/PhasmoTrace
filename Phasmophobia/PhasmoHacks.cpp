#include "PhasmoTrace/Tracer.hpp"
#include "PhasmoTrace/TracerCapstone.hpp"
#include "PhasmoTrace/il2cpp.h"
#include "PhasmoTrace/magic_enum.hpp"
#include "PhasmoTrace/Game/Phasmophobia.hpp"
#include "PhasmoTrace/Random.hpp"
#include "PhasmoTrace/Timer.hpp"
#include "QuickDebug.hpp"
#include "imgui.h"

#include <string_view>
#include <sstream>
#include <unordered_map>
#include <optional>
#include <stack>
#include <utility>
#include <cmath>

using namespace Phasmophobia;



// Basic definitions
extern "C" {
std::string_view PT_pluginName = "Basic Phasmophobia hack collection",
                 PT_pluginVersion = "V1.1",
                 PT_pluginDescription = "A collection of a few Phasmophobia hacks I have written.";

// Globals
PhasmoTrace::Tracer *tracer;
}

// Settings
static PhasmoTrace::RandomGenerator rng;
static bool isHost = true,
            inGame = false,
            unlimitedStamina = false,
            invisibility = false,
            hidePlayerModelToo = false,
            noFootsteps = false,
            freeCam = false,
            readyUpToStart = false,
            keepTopGhostState = false,
            noDeath = false,
            noKick = false,
            noInventoryItemRemoval = false,
            hideIDs = false,
            forceGhostAbilityOnce = false,
            forceGhostInteractionOnce = false,
            forceTruckButtonUsability = false,
            autoTakeOwnership = false,
            photoCamLimitsRemoved = false,
            whiteSageLimitsRemoved = false,
            candleLimitsRemoved = false,
            itemsDisturb = false,
            doorSmash = false,
            allowGrabAsDead = false,
            forceVisibility = false,
            blockLevelExit = false,
            mapJumping = false,
            nonLocalMapJumping = false,
            skipNextMapJump = false,
            seedOverride = false,
            spawnAllCursedPosessions = false;
static PhasmoTrace::Timer gameStartTimer;
static int seedOverrideValue;
static unsigned doorUpdateCounter = 0,
                doorUpdateVariation;
static QuickDebug::Addr serverManager = 0,
                        stringKlass = 0,
                        il2CppArrayKlass = 0;
static char customRoomName[512];
static std::string currentMap;
static std::optional<GhostState> lastKnownGhostState;
static std::stack<std::pair<GhostState, bool>> ghostStatesToSet;
static std::vector<std::string> mapNames = {
    "SplashScreen_Headphones",
    "Menu_New",
    "Willow_Street_House",
    "Ridgeview_Road_House",
    "Bleasdale_Farmhouse",
    "Brownstone_High_School",
    "Edgefield_Street_House",
    "Grafton_Farmhouse",
    "Maple_Lodge_Campsite",
    "Tanglewood_Street_House",
    "Asylum",
    "Prison"
};
static Il2Cpp::UnityEngine::Vector3 freeCamOffset{0.0f, 0.0f, 0.0f};

// Helpers
void setGhostState(GhostState state, bool keepState = false) noexcept {
    ghostStatesToSet.push({state, keepState});
}

std::vector<Item> items = {
    Item("HandCamera", "Photo Camera", 3),
    Item("EMFReader", "EMF Reader", 2),
    Item("ParabolicMicrophone", "Parabolic Microphone", 2, 1),
    Item("PainKillers", "Sanity Pills", 4),
    Item("Thermometer", "Thermometer", 3, 1),
    Item("Torch", "Flashlight", 10, "Torch$$Use"),
    Item("EVPRecorder", "Spirit Box", 2, 2),
    Item("WhiteSage", "Smudge Stick", 4, "WhiteSage$$Use"),
    Item("Candle", "Candle", Item::dynamicInstanceCount, "Candle$$Use") // Needs to be used once to be found
};
Item& getItem(std::string_view name) {
    for (auto& item : items) {
        if (item.name == name) {
            return item;
        }
    }
    throw std::runtime_error("Unknown item requested");
}

void itemDisturbTrigger() {
    if (!itemsDisturb) {
        return;
    }
    // Get random item
    const auto& item = items[rng.getUInt(items.size()-1)];
    // Disturb it
    tracer->addToOnUpdateQueue([&item] () {
        if (item.disturbed) {
            item.use(rng.getUInt(item.instances.size()-1));
        }
        tracer->addToActionQueue(itemDisturbTrigger);
    });
};

QuickDebug::Addr findLevelLoader() {
    static QuickDebug::Addr cache = 0;
    if (cache) {
        return cache;
    } else {
        for (const auto& brkl : tracer->getTarget().getBreakpoints()) {
            for (const auto& brk : brkl.second.getVector()) {
                const auto& signature = std::any_cast<const PhasmoTrace::Function*>(brk->getData())->signature;
                if (signature == "void Photon_Pun_PhotonNetwork__LoadLevel (System_String_o* levelName, const MethodInfo* method);") {
                    cache = brkl.second.getAddr();
                    return cache;
                }
            }
        }
        return 0; // Should never happen
    }
}

void overrideSeed() {
    tracer->call("UnityEngine.Random$$InitState", {reinterpret_cast<QuickDebug::Word&>(seedOverrideValue)});
}

void sendRPC(QuickDebug::Addr view, std::string_view name, int actorNo, QuickDebug::Addr arg) {
    if (!stringKlass || !il2CppArrayKlass) {
        return;
    }
    auto rpcNameBuf = tracer->getTempBufferSpace();
    tracer->lateAllocateTempBufferSpace(tracer->storeStringAt(rpcNameBuf, name, stringKlass));
    auto parametersBuf = tracer->allocateTempBufferSpace(sizeof(Il2Cpp::BuiltIn::Il2CppArray));
    Il2Cpp::BuiltIn::Il2CppArray parameters;
    parameters.obj.klass = il2CppArrayKlass;
    parameters.arrayBounds = 0;
    parameters.max_length = arg?1:0;
    parameters.first_item = arg;
    tracer->getTarget().writeObject(parametersBuf, parameters);
    tracer->call(tracer->getTarget().getBreakpointsByName("Photon.Pun.PhotonView$$RPC").at(0)->getAddr(), {view, rpcNameBuf, static_cast<QuickDebug::Word>(actorNo), parametersBuf, 0x0});
}

void loadScene(std::string_view mapName) {
    tracer->activateBreakpointWithName("ServerManager$$LoadScene");
    auto mapNameBuf = tracer->getTempBufferSpace();
    tracer->lateAllocateTempBufferSpace(tracer->storeStringAt(mapNameBuf, mapName, stringKlass));
    auto serverManagerObject = tracer->getTarget().readObject<Il2Cpp::Object<Il2Cpp::Game::ServerManager>>(serverManager).fields;
    sendRPC(serverManagerObject.photonView, "LoadScene", 6, mapNameBuf);
}

// Callbacks
extern "C" {
void init() noexcept {
    initGameBase(tracer);
    rng.seed();
    // Find function that triggers the items
    for (auto& item : items) {
        item.findTrigger();
    }
    tracer->activateBreakpointWithName("ServerManager$$Start");
    tracer->activateBreakpointWithName("ExitLevel$$Exit");
    for (const auto& item : items) {
        tracer->activateBreakpointWithName(item.name+"$$"+item.startFunctionName);
    }
    tracer->activateBreakpointWithName("GhostAI$$ChangeState");
    tracer->activateBreakpointWithName("GhostActivity$$Update");
    tracer->activateAllBreakpointsWithName("Photon.Pun.PhotonView$$RPC");
    tracer->activateBreakpointWithName("ServerManager$$LoadScene");
}

void deinit() {
    if (itemsDisturb) {
        throw std::runtime_error("Can't unload while disturbing items");
    }
}

void onBreakpoint(QuickDebug::BreakpointList& brkl) {
    // Handle breakpoints
    auto brk = brkl.getVector()[0];
    std::string_view signature;
    if (brk->getData().type() == typeid(const PhasmoTrace::Function*)) {
        signature = std::any_cast<const PhasmoTrace::Function*>(brk->getData())->signature;
    }
    if (brk->getName() == "ExitLevel$$Exit") {
        for (auto& item : items) {
            item.clearInstances();
        }
        inGame = false;
        currentMap.clear();
        return;
    }
    if (brk->getName() == "ServerManager$$Start") {
        for (auto& item : items) {
            item.clearInstances();
        }
        inGame = false;
        currentMap.clear();
        serverManager = tracer->getTarget().getRegisters().rcx;
        return;
    }
    if (brk->getName() == "ServerManager$$LoadScene") {
        inGame = true;
        gameStartTimer.reset();
        currentMap = tracer->getStrAt(tracer->getTarget().getRegisters().rdx);
    }
    for (auto& item : items) {
        if (brk->getName() == item.name+"$$"+item.startFunctionName) {
            const auto& regs = tracer->getTarget().getRegisters();
            if (item.startFunctionName != "Start" || item.startFunctionName != ".ctor") {
                // It may be called more than once, make sure to not double-list it
                for (const auto instance : item.instances) {
                    if (instance == regs.rcx) {
                        return;
                    }
                }
            }
            item.nextInstance() = regs.rcx;
            return;
        }
    }
    if (unlimitedStamina && brk->getName() == "PlayerStamina$$StartDraining") {
        tracer->retOut(brk);
        return;
    }
    if (skipNextMapJump && brk->getName() == "ServerManager$$LoadScene") {
        tracer->retOut(brk);
        skipNextMapJump = false;
        return;
    }
    if (brk->getName() == "Photon.Pun.PhotonView$$RPC") {
        const auto& regs = tracer->getTarget().getRegisters();
        if (autoTakeOwnership) {
            auto view = regs.rcx;
            tracer->addToOnUpdateQueue([view] () mutable {
                tracer->call("Photon.Pun.PhotonView$$RequestOwnership", {view});
            });
        }
        if (invisibility) {
            auto name = tracer->getStrAt(regs.rdx);
            if (name == "SetPlayerName" || name == "SetPlayerInformation" || name == "AddPlayer" || (hidePlayerModelToo && name == "LoadPlayerModel" || (noFootsteps && (name == "SyncWalk" || name == "SyncSprint")))) {
                // Abort RPC
                tracer->retOut(brk);
            }
        }
        if (blockLevelExit) {
            auto name = tracer->getStrAt(regs.rdx);
            if (name == "Exit") {
                // Abort RPC
                tracer->retOut(brk);
            }
        }
        // Get klasses
        if (!il2CppArrayKlass) {
            il2CppArrayKlass = tracer->getTarget().readObject<Il2Cpp::BuiltIn::Il2CppArray>(regs.r9).obj.klass;
        }
        if (!stringKlass) {
            stringKlass = tracer->getTarget().readObject<Il2Cpp::Object<Il2Cpp::System::String>>(regs.rdx).object.klass;
        }
        return;
    }
    if (noKick && brk->getName() == "ServerManager$$KickPlayerNetworked") {
        tracer->retOut(brk);
        return;
    }
    if (freeCam && brk->getName() == "UnityEngine.Transform$$set_localPosition") {
        const auto& regs = tracer->getTarget().getRegisters();
        auto pos = tracer->getTarget().readObject<Il2Cpp::UnityEngine::Vector3>(regs.rdx);
        pos.x = freeCamOffset.x;
        pos.y = freeCamOffset.y;
        pos.z = freeCamOffset.z;
        tracer->getTarget().writeObject(regs.rdx, pos);
        return;
    }
    if ((hideIDs || customRoomName[0]) && brk->getName() == "Photon.Pun.PhotonNetwork$$CreateRoom") {
        if (hideIDs) {
            auto roomOptionsBuf = tracer->getTarget().getRegisters().rdx;
            auto roomOptions = tracer->getTarget().readObject<Il2Cpp::Object<Il2Cpp::Photon::Realtime::RoomOptions>>(roomOptionsBuf);
            roomOptions.fields.publishUserId = false;
            tracer->getTarget().writeObject(roomOptionsBuf, roomOptions);
        }
        if (customRoomName[0]) {
            // Making a temp buffer may or may not be safer here (idk how long the pointer is going to be kept)
            auto roomNameBuf = tracer->getTarget().getRegisters().rcx;
            stringKlass = tracer->getTarget().readObject<Il2Cpp::Object<Il2Cpp::BuiltIn::Empty>>(roomNameBuf).object.klass;
            tracer->storeStringAt(roomNameBuf, customRoomName, stringKlass);
        }
        return;
    }
    if (readyUpToStart && brk->getName() == "ServerManager$$Ready") {
        auto regs = tracer->getTarget().getRegisters();
        regs.rip = tracer->getTarget().getBreakpointsByName("ServerManager$$StartGame")[0]->getAddr();
        tracer->getTarget().setRegisters(regs);
        return;
    }
    if (noDeath && brk->getName() == "Player$$StartKillingPlayer") {
        setGhostState(GhostState::normalEvent); // Let's at least try to scare the player a little :P
        tracer->retOut(brk);
        return;
    }
    if (noInventoryItemRemoval && brk->getName() == "InventoryManager$$RemoveItemsFromInventory") {
        tracer->retOut(brk);
        return;
    }
    if (forceGhostAbilityOnce && brk->getName() == "GhostActivity$$Update") {
        auto regs = tracer->getTarget().getRegisters();
        regs.rip =  tracer->getTarget().getBreakpointsByName("GhostActivity$$GhostAbility")[0]->getAddr();
        tracer->getTarget().setRegisters(regs);
        forceGhostAbilityOnce = false;
        return;
    }
    if (forceGhostInteractionOnce && brk->getName() == "GhostActivity$$Update") {
        auto regs = tracer->getTarget().getRegisters();
        regs.rip =  tracer->getTarget().getBreakpointsByName("GhostActivity$$Interact")[0]->getAddr();
        tracer->getTarget().setRegisters(regs);
        forceGhostInteractionOnce = false;
        return;
    }
    if (doorSmash && brk->getName() == "Door$$Update") {
        std::string_view callTo;
        switch (doorUpdateCounter++) {
        //case 0: callTo = "Door$$HuntingCloseDoor"; break;  Triggers "Hacker detection"
        case 1: callTo = "Door$$LockDoor"; break;
        case 20: callTo = "Door$$UnlockDoor"; break;
        case 40: callTo = "Door$$GhostForceDoor"; break;
        }
        if (doorUpdateCounter == 70 + (doorUpdateVariation % 10)) {
            doorUpdateCounter = 0;
            doorUpdateVariation++;
            return;
        }
        if (!callTo.empty()) {
            auto regs = tracer->getTarget().getRegisters();
            regs.rip = tracer->getTarget().getBreakpointsByName(callTo)[0]->getAddr();
            tracer->getTarget().setRegisters(regs);
        }
        return;
    }
    if (allowGrabAsDead && brk->getName() == "PCPropGrab$$PlayerDied") {
        tracer->retOut(brk);
        return;
    }
    if (forceVisibility) {
        for (const std::string& class_ : {"Photon.Realtime.Room", "Photon.Realtime.RoomInfo"}) {
            if (brk->getName() == class_+"$$get_IsVisible"
             || brk->getName() == class_+"$$get_IsOpen") {
                tracer->retOutWValue(brk, true);
            } else if (brk->getName() == class_+"$$get_PlayerCount") {
                tracer->retOutWValue(brk, 0);
            } else if (brk->getName() == class_+"$$get_MaxPlayers") {
                tracer->retOutWValue(brk, 4);
            } else {
                auto regs = tracer->getTarget().getRegisters();
                if (brk->getName() == class_+"$$set_IsVisible"
                 || brk->getName() == class_+"$$set_IsOpen") {
                    regs.rdx = true;
                } else if (brk->getName() == class_+"$$set_PlayerCount") {
                    regs.rdx = 0;
                } else if (brk->getName() == class_+"$$set_MaxPlayers") {
                    regs.rdx = 4;
                }
                tracer->getTarget().setRegisters(regs);
            }
        }
        return;
    }
    if (autoTakeOwnership && brk->getName() == "Photon.Pun.PhotonView$$TransferOwnership") {
        tracer->retOut(brk);
        return;
    }
    if (signature == "void Photon_Pun_PhotonNetwork__LoadLevel (System_String_o* levelName, const MethodInfo* method);") {
        if (seedOverride) {
            tracer->addToOnUpdateQueue(overrideSeed);
        }
        return;
    }
    if (forceTruckButtonUsability && brk->getName() == "ExitLevel$$ThereAreAlivePlayersOutsideTheTruck") {
        tracer->retOutWValue(brk, false);
        return;
    }
    if (seedOverride && brk->getName() == "UnityEngine.Random$$InitState") {
        auto regs = tracer->getTarget().getRegisters();
        regs.rcx = seedOverrideValue;
        return;
    }
    if (brk->getName() == "GhostAI$$ChangeState") {
        auto regs = tracer->getTarget().getRegisters();
        if (!ghostStatesToSet.empty() && lastKnownGhostState.has_value()) {
            if (lastKnownGhostState.value() == GhostState::favouriteRoom) {
                const auto& ghostStateToSet = ghostStatesToSet.top();
                regs.rdx = magic_enum::enum_integer(ghostStateToSet.first);
                if (!ghostStateToSet.second) {
                    ghostStatesToSet.pop();
                }
            } else {
                regs.rdx = magic_enum::enum_integer(GhostState::favouriteRoom);
            }
            tracer->getTarget().setRegisters(regs);
        }
        lastKnownGhostState = static_cast<GhostState>(regs.rdx);
        return;
    }
}

void onUIRender() noexcept {
    using namespace ImGui;
    // Periodically make sure all players are on map if needed
    if (gameStartTimer.get<std::chrono::minutes>() > 1) {
        if (forceVisibility && !currentMap.empty()) {
            static PhasmoTrace::Timer timer;
            if (timer.get<std::chrono::seconds>() > 1) {
                timer.reset();
                skipNextMapJump = true;
                tracer->addToOnUpdateQueue([] () {loadScene(currentMap);});
            }
        }
    }
    // Properties
    TextUnformatted("Fill these in accordingly");
    Checkbox("I am the host", &isHost);
    if (Checkbox("In game", &inGame) && inGame) {
        gameStartTimer.reset();
    }
    // General stuff
    Separator();
    TextUnformatted("General stuff");
    if (Checkbox("Unlimited stamina", &unlimitedStamina) && unlimitedStamina) {
        tracer->activateBreakpointWithName("PlayerStamina$$StartDraining");
    }
    if (Checkbox("Hide from player list", &invisibility) && invisibility) {
        //...
    }
    if (invisibility) {
        Checkbox("- Hide player model too", &hidePlayerModelToo);
        Checkbox("- Disable footsteps", &noFootsteps);
    }
    if (Checkbox("No kick", &noKick) && noKick) {
        tracer->activateBreakpointWithName("ServerManager$$KickPlayerNetworked");
    }
    if (Checkbox("Free camera", &freeCam) && freeCam) {
        tracer->activateBreakpointWithName("UnityEngine.Transform$$set_localPosition");
    }
    if (freeCam) {
        TextUnformatted("Offset: ");
        InputFloat("X", &freeCamOffset.x, 0.5f);
        InputFloat("Y", &freeCamOffset.y, 0.5f);
        InputFloat("Z", &freeCamOffset.z, 0.5f);
    }
    if (Checkbox("Override random seed", &seedOverride) && seedOverride) {
        tracer->addToOnUpdateQueue(overrideSeed);
        tracer->activateBreakpointWithName("UnityEngine.Random$$InitState");
        tracer->activateBreakpointWithName("Photon.Pun.PhotonNetwork$$LoadLevel");
    }
    if (seedOverride) {
        SameLine();
        if (InputInt("Value", &seedOverrideValue)) {
            tracer->addToOnUpdateQueue(overrideSeed);
        }
    }
    if (Button("Join random lobby")) {
        tracer->addToOnUpdateQueue([] () {
            tracer->call(tracer->findBreakpointBySignature("bool Photon_Pun_PhotonNetwork__JoinRandomRoom (const MethodInfo* method);")->getAddr(), {0});
            isHost = false;
        });
    }
    if (isHost) {
        if ((InputText("Custom room name", customRoomName, sizeof(customRoomName))) + (Checkbox("Hide IDs (fixme)", &hideIDs) && hideIDs)) {
            tracer->activateBreakpointWithName("Photon.Pun.PhotonNetwork$$CreateRoom");
        }
        if (Checkbox("Ready up to start", &readyUpToStart) && readyUpToStart) {
            tracer->activateBreakpointWithName("ServerManager$$Ready");
        }
        if (Checkbox("No death", &noDeath) && noDeath) {
            tracer->activateBreakpointWithName("Player$$StartKillingPlayer");
        }
        if (Button("Kill player v.1")) {
            tracer->addToOnUpdateQueue([] () {
                auto& target = tracer->getTarget();
                auto regs = target.getRegisters();
                regs.rip = target.getBreakpointsByName("Player$$KillPlayer")[0]->getAddr();
                target.setRegisters(regs);
            });
        }
        SameLine();
        if (Button("Kill player v.2")) {
            tracer->addToOnUpdateQueue([] () {
                auto& target = tracer->getTarget();
                auto regs = target.getRegisters();
                regs.rip = target.getBreakpointsByName("Player$$StartKillingPlayer")[0]->getAddr();
                target.setRegisters(regs);
            });
        }
        SameLine();
        if (Button("Drop props")) {
            tracer->addToOnUpdateQueue([] () {
                auto& target = tracer->getTarget();
                auto regs = target.getRegisters();
                regs.rip = target.getBreakpointsByName("Player$$ForceDropPropsSync")[0]->getAddr();
                target.setRegisters(regs);
            });
        }
        if (Button("Force ghost ability")) {
            forceGhostAbilityOnce = true;
        }
        SameLine();
        if (Button("Force ghost interaction")) {
            forceGhostInteractionOnce = true;
        }
        if (Checkbox("Force truck button usability", &forceTruckButtonUsability) && forceTruckButtonUsability) {
            tracer->activateBreakpointWithName("ExitLevel$$ThereAreAlivePlayersOutsideTheTruck");
        }
        if (Checkbox("Force lobby visibility", &forceVisibility) && forceVisibility) {
            for (const std::string& class_ : {"Photon.Realtime.Room", "Photon.Realtime.RoomInfo"}) {
                for (const std::string& settergetter : {"get",  "set"}) {
                    for (const std::string& name : {"IsVisible", "IsOpen", /*"PlayerCount", */"MaxPlayers"}) {
                        auto function = class_+"$$"+settergetter+"_"+name;
                        if (function != "Photon.Realtime.Room$$get_PlayerCount") {
                            tracer->activateBreakpointWithName(function);
                        }
                    }
                }
            }
        }
        if (Checkbox("Block level exit", &blockLevelExit) && blockLevelExit) {
            tracer->activateBreakpointWithName("ExitLevel$$StartAttemptExitLevel");
        }
        /*if (Checkbox("Spawn all cursed posessions", &spawnAllCursedPosessions) && spawnAllCursedPosessions) {
            activateBreakpointWithName("CursedItemsController$$Start");
            for (const auto& posession : posessions) {
                activateBreakpointWithName(posession.name+"$$"+posession.spawnPointSetterName);
            }
        }*/
    }
    if (Checkbox("Don't loose items when dead", &noInventoryItemRemoval) && noInventoryItemRemoval) {
        tracer->activateBreakpointWithName("InventoryManager$$RemoveItemsFromInventory");
    }
    if (Checkbox("Allow grabbing everything when dead", &allowGrabAsDead) && allowGrabAsDead) {
        tracer->activateBreakpointWithName("PCPropGrab$$PlayerDied");
    }
    if (Checkbox("Try to take ownership of stuff", &autoTakeOwnership) && autoTakeOwnership) {
        //...
    }
    TextUnformatted("Remove some limits:");
    BeginDisabled(photoCamLimitsRemoved);
    SameLine();
    if (Checkbox("Photo Camera##534", &photoCamLimitsRemoved) && photoCamLimitsRemoved) {
        tracer->addToActionQueue([] () {
            tracer->overrideBranches(getItem("HandCamera").trigger, 240, {false, true, false, true});
        });
    }
    EndDisabled();
    BeginDisabled(whiteSageLimitsRemoved);
    SameLine();
    if (Checkbox("Smudge Stick##764", &whiteSageLimitsRemoved) && whiteSageLimitsRemoved) {
        tracer->addToActionQueue([] () {
            tracer->overrideBranches(getItem("WhiteSage").trigger, 240, {false, false, false, true});
        });
    }
    EndDisabled();
    BeginDisabled(candleLimitsRemoved);
    SameLine();
    if (Checkbox("Candle##983", &candleLimitsRemoved) && candleLimitsRemoved) {
        tracer->addToActionQueue([] () {
            tracer->overrideBranches(getItem("Candle").trigger, 240, {false, true, true, true});
        });
    }
    EndDisabled();
    if (!itemsDisturb)  {
        TextUnformatted("Use random instance of item:");
        for (const auto& item : items) {
            SameLine();
            BeginDisabled(!item.isUsable());
            if (Button(item.friendlyName.c_str())) {
                tracer->addToOnUpdateQueue([&item] () {item.use(rng.getUInt(item.instances.size()-1));});
            }
            EndDisabled();
        }
    } else {
        TextUnformatted("Items to disturb:");
        for (auto& item : items) {
            SameLine();
            Checkbox(item.friendlyName.c_str(), &item.disturbed);
        }
    }
    if (Checkbox("Disturb items", &itemsDisturb) && itemsDisturb) {
        tracer->addToActionQueue(itemDisturbTrigger);
    }
    if (Checkbox("Disturb doors", &doorSmash) && doorSmash) {
        tracer->activateBreakpointWithName("Door$$Update");
    }
    // Map jumping
    Separator();
    BeginDisabled(!serverManager || !stringKlass);
    if (Checkbox("Enable map jumping", &mapJumping)) {
        //...
    }
    if (mapJumping) {
        if (isHost) {
            Checkbox("Remote-only map jumping", &nonLocalMapJumping);
            TextUnformatted("Jump to:");
            for (const auto& mapName : mapNames) {
                if (Button(mapName.c_str())) {
                    if (nonLocalMapJumping) {
                        skipNextMapJump = true;
                    }
                    tracer->addToOnUpdateQueue([&] () {loadScene(mapName);});
                }
            }
        } else {
            BeginDisabled();
            bool localMapJumping = true;
            Checkbox("Local-only map jumping", &localMapJumping);
            EndDisabled();
            TextUnformatted("Jump to:");
            for (const auto& mapName : mapNames) {
                if (Button(mapName.c_str())) {
                    tracer->addToOnUpdateQueue([&] () {
                        auto strBuf = tracer->getTempBufferSpace();
                        tracer->lateAllocateTempBufferSpace(tracer->storeStringAt(strBuf, mapName));
                        tracer->call(findLevelLoader(), {strBuf});
                    });
                }
            }
        }
    }
    EndDisabled();
    // Ghost AI stuff
    if (isHost) {
        Separator();
        TextUnformatted("Ghost state manipulation");
        if (!ghostStatesToSet.empty()) {
            if (Button("Reset state stack")) {
                while (!ghostStatesToSet.empty()) {
                    ghostStatesToSet.pop();
                }
            }
        }
        if (!ghostStatesToSet.empty()) {
            keepTopGhostState = ghostStatesToSet.top().second;
            if (Checkbox("Keep state", &keepTopGhostState)) {
                ghostStatesToSet.top().second = keepTopGhostState;
            }
        } else {
            Checkbox("Keep state", &keepTopGhostState);
        }
        for (const auto state : {GhostState::idle, GhostState::hunt, GhostState::appear, GhostState::summon}) {
            SameLine();
            if (Button(magic_enum::enum_name(state).data())) {
                setGhostState(state, keepTopGhostState);
            }
        }
    }
    // Some debug info
    Separator();
    TextUnformatted("Helpful sideinfos");
    if (isHost) {
        if (lastKnownGhostState.has_value()) {
            Text("Ghost state: %s", magic_enum::enum_contains(lastKnownGhostState.value()) ?
                                                              magic_enum::enum_name(lastKnownGhostState.value()).data() :
                                                              ("unknown ("+std::to_string(magic_enum::enum_integer(lastKnownGhostState.value()))+')').c_str());
        }
    }
    // Experiments
#   ifndef NDEBUG
    if (Button("Run experiment")) {
        tracer->addToOnUpdateQueue([] () {
            // Create position and rotation
            Il2Cpp::UnityEngine::Vector3 pos;
            Il2Cpp::UnityEngine::Quaternion rot;
            uint8_t group;
            Il2Cpp::System::Collections::Generic::List data;
            pos.z = pos.y = pos.x = 0.0f;
            rot.z = rot.y = rot.z = 0.0f;
            rot.w = 1.0f;
            group = 0;
            data._items = 0;
            data._size = 0;
            data._version = 0;
            data._syncRoot = 0;
            // Store them in buffer
            auto fnc = tracer->getTarget().getBreakpointsByName("Photon.Pun.PhotonNetwork$$InstantiateSceneObject")[0]->getAddr();
            // Statically sized data
            auto posBuf = tracer->allocateTempBufferSpace(sizeof(pos));
            auto rotBuf = tracer->allocateTempBufferSpace(sizeof(rot));
            auto groupBuf = tracer->allocateTempBufferSpace(sizeof(group));
            auto dataBuf = tracer->allocateTempBufferSpace(sizeof(data));
            tracer->getTarget().writeObject(posBuf, pos);
            tracer->getTarget().writeObject(rotBuf, rot);
            tracer->getTarget().writeObject(groupBuf, group);
            tracer->getTarget().writeObject(dataBuf, data);
            // Dynamically sized data
            auto strBuf = tracer->getTempBufferSpace();
            tracer->lateAllocateTempBufferSpace(tracer->storeStringAt(strBuf, "DeadPlayerBody"));
            auto codeBuf = tracer->getTempBufferSpace();
            // Generate calling code
            auto code = tracer->generateABICall(fnc, {strBuf, posBuf, rotBuf, groupBuf, dataBuf, 0});
            // Store calling code
            tracer->getTarget().writeBuffer(codeBuf, code.data(), code.size());
            tracer->lateAllocateTempBufferSpace(code.size());
            // Jump into calling code
            auto regs = tracer->getTarget().getRegisters();
            regs.rip = codeBuf;
            tracer->getTarget().setRegisters(regs);
        });
    }
#   endif
}

void onHTTPTrigger(const std::string& trigger, int param) {
    if (trigger == "queueghoststate" && ghostStatesToSet.empty() && magic_enum::enum_contains<GhostState>(param)) {
        setGhostState(static_cast<GhostState>(param));
    } else if (trigger == "forceghost") {
        switch (param) {
        case 0: forceGhostInteractionOnce = true; break;
        case 1: forceGhostAbilityOnce = true; break;
        }
    } else if (trigger == "forcekick") {
        tracer->addToOnUpdateQueue([] () {
            skipNextMapJump = true;
            loadScene("SplashScreen_Headphones");
        });
    }
}

std::string *insideHTTPOverview() {
    if (isHost) {
        std::ostringstream os;
        os << "<h2>Scene loading</h2>"
              "<p><a href='forcekick/0'>Force kick everyone</a></p>"
              "<h2>Ghost state</h2>";
        if (lastKnownGhostState.has_value()) {
            os << "<p>Ghost is currently " << magic_enum::enum_name(lastKnownGhostState.value()) << "ing.</p>";
        }
        os << "<p>Trigger: ";
        for (const auto state : {GhostState::normalEvent, GhostState::hunt, GhostState::summon}) {
            os << "<a href='queueghoststate/" << std::to_string(magic_enum::enum_integer(state)) << "'>Ghost " << magic_enum::enum_name(state) << "</a>  ";
        }
        os << "<a href='forceghost/0'>Ghost interaction</a>  "
              "<a href='forceghost/1'>Ghost ability</a>  ";
        os << "</p>";
        return new auto(os.str());
    } else {
        return nullptr;
    }
}
}
