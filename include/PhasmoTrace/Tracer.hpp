#ifndef TRACER_HPP
#define TRACER_HPP
#include "Namespace.hpp"
#include "Plugin.hpp"
#include "QuickDebug.hpp"
#include "logger.hpp"
#include "il2cpp.h"
#include "Timer.hpp"

#include <iostream>
#include <string>
#include <string_view>
#include <sstream>
#include <vector>
#include <memory>
#include <utility>
#include <mutex>
#include <queue>
#include <variant>
#include <functional>
#include <thread>
#include <csignal>
#include <fmt/format.h>



namespace PhasmoTrace {
struct Function {
    uint64_t addr;
    std::string name,
                signature;
};


inline
bool isGoodStr(std::string_view str) {
    std::string_view goodChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789$._";
    for (const char c : str) {
        if (goodChars.find_first_of(c) == goodChars.npos) {
            return false;
        }
    }
    return true;
}


struct FunctionPointerFinder {
    QuickDebug::Addr &target;
    std::variant<unsigned, std::string_view> identifier;
};


struct Il2CppHelpers {
    QuickDebug::Addr il2cpp_class_from_name = 0,
                     il2cpp_value_box = 0,
                     il2cpp_get_corlib = 0;
};


class Tracer {
    Logger<Tracer> logger;
    Target target;
    Game game;
    std::vector<Function> knownFunctions;
    bool breakpointsReady = false;
    std::vector<std::function<void()>> actionQueue;
    std::queue<std::function<void()>> onUpdateQueue;
    std::mutex actionQueueMutex, onUpdateQueueMutex;
    std::vector<PluginDesc> plugins;
    std::string tracerLog;
    QuickDebug::Addr addrBase = 0,
                     bufferSpace = 0,
                     bufferSpaceAllocOff;
    Il2CppHelpers il2CppHelpers;
    std::function<void()> returnCatch = nullptr;

    void runActionQueue() {
        std::scoped_lock L(actionQueueMutex);
        for (const auto& fnc : actionQueue) {
            fnc();
        }
        actionQueue.clear();
    }

    void initializeIl2CppHelpers(const char *path);

public:
    Tracer(const std::string& fpath) : game(fpath) {
        logger.inst_ptr = this;
    }

    void addToActionQueue(const std::function<void()>& fnc) {
        std::scoped_lock L(actionQueueMutex);
        actionQueue.push_back(fnc);
    }
    void addToOnUpdateQueue(const std::function<void()>& fnc) {
        std::scoped_lock L(onUpdateQueueMutex);
        onUpdateQueue.push(fnc);
    }
    void delayedAddToActionQueue(const std::function<void()>& fnc) {
        addToOnUpdateQueue([this, fnc] () {
            addToActionQueue(fnc);
        });
    }
    void delayedAddToOnUpdateQueue(const std::function<void()>& fnc) {
        addToActionQueue([this, fnc] () {
            addToOnUpdateQueue(fnc);
        });
    }

    // Note: must be called from outside action queue
    void delay(unsigned msecs, const std::function<void()>& cb, Timer *timer = nullptr) {
        if (!timer) {
            timer = new Timer;
        }
        if (timer->get() < msecs) {
            delayedAddToOnUpdateQueue([this, msecs, cb, timer] () {
                delay(msecs, cb, timer);
            });
        } else {
            delete timer;
            cb();
        }
    }

    Target& getTarget() {
        return target;
    }

    const std::vector<Function>& getKnownFunctions() const {
        return knownFunctions;
    }

    // Note: only use this space for very small amounts of data. The buffer space size is UNDEFINED
    QuickDebug::Addr getTempBufferSpace() const {
        return bufferSpace + bufferSpaceAllocOff;
    }
    void lateAllocateTempBufferSpace(size_t size) {
        bufferSpaceAllocOff += size;
    }
    QuickDebug::Addr allocateTempBufferSpace(size_t size) {
        auto fres = bufferSpace + bufferSpaceAllocOff;
        bufferSpaceAllocOff += size;
        return fres;
    }
    void resetTempBufferSpaceAllocs() {
        bufferSpaceAllocOff = 0;
    }

    template<class T>
    Il2Cpp::Object<T> boxIl2CppValue(const std::string& namespaze, const std::string& name, const T& value) {
        auto buffer = getTempBufferSpace();
        target.writeObject(buffer, value);
        auto boxedBuf = boxIl2CppValue(namespaze, name, buffer);
        return target.readObject<Il2Cpp::Object<T>>(boxedBuf);
    }

    QuickDebug::Addr getIl2CppKlass(const std::string& namespaze, const std::string& name);
    QuickDebug::Addr boxIl2CppValue(const std::string& namespaze, const std::string& name, QuickDebug::Addr valueBuf);

    void retOut(std::shared_ptr<QuickDebug::Breakpoint>& brk);
    void retOutWValue(std::shared_ptr<QuickDebug::Breakpoint>& brk, QuickDebug::Word value);

    QuickDebug::Addr findRelativeFunctionAddressByName(std::string_view name) const;

    std::shared_ptr<QuickDebug::Breakpoint> findBreakpointBySignature(std::string_view signature) noexcept;
    void forEachBreakpointWithStrInName(std::string_view str, const std::function<void(const std::shared_ptr<QuickDebug::Breakpoint>&)>& cb) noexcept;
    void activateEachBreakpointWithStrInName(std::string_view str) noexcept;
    void activateBreakpointWithName(std::string_view str) noexcept;
    void activateBreakpointWithSignature(std::string_view str) noexcept;
    void activateAllBreakpointsWithName(std::string_view str) noexcept;
    void deactivateBreakpointWithName(std::string_view str) noexcept;
    void deactivateAllBreakpointsWithName(std::string_view str) noexcept;

    void findFunctionPointers(QuickDebug::Addr addr, size_t size, const std::vector<FunctionPointerFinder>& finders);
    void overrideBranches(QuickDebug::Addr addr, size_t size, const std::vector<bool>& overrides);
    void logEvent(std::string_view message) noexcept;
    void loadPlugin(const std::string& fname);
    size_t storeStringAt(Addr addr, std::string_view str, QuickDebug::Addr klass = 0);
    std::string getStrAt(Addr addr);
    std::string getValueString(uint64_t val);
    std::vector<uint8_t> generateABICall(QuickDebug::Addr fnc, const std::vector<QuickDebug::Word>& args, std::function<void(std::ostringstream&)> beforeCallGen = nullptr, std::function<void(std::ostringstream&)> beforeReturnGen = nullptr);
    std::vector<uint8_t> assemble(std::string_view assembly);
    void call(std::string_view fnc, const std::vector<QuickDebug::Word>& args);
    void call(QuickDebug::Addr fnc, const std::vector<QuickDebug::Word>& args);
    void continueUntilReturn();
    void loadScript(std::string_view filename);
    void UIBreakpointButton(const std::shared_ptr<Breakpoint>& breakpoint) noexcept;
    void runUI() noexcept;
    void runHTTP();
    void run(QuickDebug::pid_t pid, const char *gameassembly_path);
    void patch_gameassembly(std::string_view gameassembly_path);
};
}
#endif // TRACER_HPP
